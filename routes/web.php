<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
//Route::get('/', 'InsuranceController@index');
Route::post('/checkout', 'InsuranceController@checkout');
Route::post('/payment', 'InsuranceController@payment');
Route::get('/payment/success', 'InsuranceController@paymentSuccess');

Route::any('/findInsurance', 'InsuranceController@search');
Route::any('/findFlight', 'FlightsController@search');

Route::get('/autocomplete/cities', 'FlightsController@getCitiesList');