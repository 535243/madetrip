<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>Made Trip - туристический оператор</title>
        <link rel="shortcut icon" href="/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" />
        <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->

        <link rel="stylesheet" type="text/css" href="{{ asset('css/select2.min.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />

        <!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/autocomplete.css') }}"> -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
        <script src="{{ asset("js/jquery.selectric.js") }}" ></script>
    </head>

    <body class="Insurance-tab">
        <div class="is--overlay is--top">
            <!-- Header -->
            <header role="banner">
                <div class="wrapper header__wrapper">
                    <div class="branding header__branding">
                        <div class="logo header__logo">
                            <div class="svg__wrapper">
                                <a href="/">
                                    <img src="{{ asset('images/logo.png') }}">
                                </a>
                            </div>
                        </div>
                        <h5 class="branding__title">madetip.ru</h5>
                        <p class="branding__desc">Страховка для путешествия онлайн</p>
                    </div>

                    <div class="header__nav">
                        <a class="nav__reveal" href="#">
                            <span class="icon--burger"></span>
                        </a>

                        <nav class="nav-main" role="navigation">
                            <ul role="menubar">
                                <li role="menuitem" tabindex="0">
                                    <a href="#">
                                        <span>Flights</span>
                                    </a>
                                </li>
                                <li role="menuitem" tabindex="0" style="display:none;">
                                    <a href="#">
                                        <span>Hotels</span>
                                    </a>
                                </li>
                                <li role="menuitem" tabindex="0" style="display:none;">
                                    <a href="#">
                                        <span>Cars</span>
                                    </a>
                                </li>
                            </ul>

                            <ul role="menubar" aria-label="other options menu">
                                <li role="menuitem" tabindex="0">
                                    <a href="#">
                                        <span>About us</span>
                                    </a>
                                </li>
                                <li role="menuitem" tabindex="0">
                                    <a href="#">
                                        <span>How it works</span>
                                    </a>
                                </li>
                                <li role="menuitem" tabindex="0">
                                    <a href="#">
                                        <span>Help</span>
                                    </a>
                                </li>
                                <li role="menuitem" tabindex="0">
                                    <a href="#">
                                        <span>Terms of use</span>
                                    </a>
                                </li>
                                <li role="menuitem" tabindex="0">
                                    <a href="#">
                                        <span>Privacy</span>
                                    </a>
                                </li>
                                <li role="menuitem" tabindex="0">
                                    <a href="#">
                                        <span>Cookie policy</span>
                                    </a>
                                </li>
                                <li role="menuitem" tabindex="0">
                                    <a href=" " aria-controls="modal--help" aria-expanded="false" rel="modal">
                                        <span>Support</span>
                                    </a>
                                </li>
                            </ul>
                            <ul role="menubar" aria-label="user menu">
                                <li role="menuitem" aria-controls="user-menu" aria-expanded="false" rel="show">
                                    <a href="#" rel="modal" aria-expanded="false" aria-controls="modal--login-register">Login</a>
                                </li>
                            </ul>
                            <ul role="menubar" aria-label="user menu">
                                <li role="menuitem" aria-controls="user-menu" aria-expanded="false" rel="show">
                                    <a href="#" rel="modal" aria-expanded="false" aria-controls="modal--sign-up">Sign Up</a>
                                </li>
                            </ul>
                            <ul role="menu" class="panel user-menu" aria-hidden="true">
                                <li role="menuitem" tabindex="-1">
                                    <a href="#" rel="modal" aria-expanded="false" aria-controls="modal--login-register">My account</a>
                                </li>
                            </ul>
                        </nav>
                    </div>

                    @yield('header')
                    @yield('content')
                </div>
            </header>

            <!-- Main -->

            <div class="wrapper content__wrapper">
                <section class="featured" role="presentation">
                    <div class="flexslider">
                        <img src="{{ asset('images/intro04.jpg') }}" alt="">
                    </div>
                </section>
            </div>

            <!-- Footer -->
            <footer role="contentinfo">
                <div class="wrapper footer__wrapper">
                    <div class="nav--categories">
                        <ul role="menubar" id="menu-form">
                            <li role="menuitem" tabindex="0" class="active menu-form__Insurance">
                                <a href="#" data-icon="package" data-type="package">
                                    <span>Страхование</span>
                                </a>
                            </li>
                            <li role="menuitem" class="menu-form__flights disabled">
                                <a href="#" data-icon="plane" data-type="meta">
                                    <span>Авиабилеты</span>
                                </a>
                            </li>
                            <li role="menuitem" tabindex="0" class="menu-form__hotel disabled">
                                <a href="#" data-icon="hotel" data-type="hotel">
                                    <span>Отели</span>
                                </a>
                            </li>
                            <li role="menuitem" tabindex="0" class="menu-form__cars disabled">
                                <a href="#" data-icon="car" data-type="car">
                                    <span>Аренда машин</span>
                                </a>
                            </li>
                        </ul>

                        <ul role="menubar">
                            <li role="menuitem" tabindex="0" class="disabled">
                                <a href="#" data-icon="ticket-2">
                                    <span>Скидки</span>
                                </a>
                            </li>
                            <li role="menuitem" tabindex="0" class="disabled">
                                <a href="#" data-icon="weather">
                                    <span>погода</span>
                                </a>
                            </li>
                            <li role="menuitem" tabindex="0" class="disabled">
                                <a href="#" data-icon="alert">
                                    <span>уведомления</span>
                                </a>
                            </li>
                            <li role="menuitem" tabindex="0">
                                <a href="" data-icon="help" aria-controls="modal--help" aria-expanded="false" rel="modal">
                                    <span>Поддержка</span>
                                </a>
                            </li>
                            <li role="menuitem" tabindex="0" data-group="languages">
                                <a data-icon="">
                                    <span>RUS</span>
                                </a>
                                <div class="dropmenu">
                                    <ul>
                                        <li><a href="#" data-icon="us">UNITED STATES</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer>
        </div>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="{{ asset('js/pickmeup.min.js') }}"></script>
        <script src="{{ asset('js/select2.full.min.js') }}"></script>
        <script src="{{ asset('js/main.js') }}?ver=3"></script>
    </body>
</html>