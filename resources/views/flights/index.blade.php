{{ Form::open(array('url' => '/findFlight', 'method' => 'post', 'class' => 'form--search form-search__flights', 'id' => 'flightsSearchForm')) }}
<div class="search__type">
    <div role="radiogroup" data-group="flighttype">
        <div role="radio">
            <input id="roundtrip" type="radio" name="trip_type[]" value="roundtrip" checked="">
            <label for="roundtrip">Туда-обратно</label>
        </div>
        <div role="radio">
            <input id="oneway" type="radio" name="trip_type[]" value="oneway">
            <label for="oneway">В одну сторону</label>
        </div>
    </div>
</div>
<div data-group="single-dest">
    <fieldset data-name="single-field">
        <div class="search__field search__field__typeahead col col--search" data-name="origin" data-next="">
            <span class="twitter-typeahead">
                <input data-target="fly-from" data-selector="typeahead-airports" placeholder="Откуда.." type="text" value="" class="input input--search input--dropdown autocomplete__input tt-input flightsCitiesList">
            </span>
            <i data-icon="origin-2"></i>
        </div>

        <div class="search__field search__field__typeahead col col--search" data-name="destination" data-next="">
            <span class="twitter-typeahead">
                <input data-target="fly-to" data-selector="typeahead-airports" placeholder="Куда.." type="text" value="" class="input input--search autocomplete__input input--dropdown tt-input flightsCitiesList">
            </span>
            <i data-icon="location"></i>
        </div>

        <div class="search__field col col--search calendar calendar__departure" data-name="depart-date" data-next="">
            <input  data-selector="pickmeup" data-type="departure" data-min="" type="text" placeholder="Туда.." class="input date__from date__from-airtorts input--search input--dropdown" name="departure_date[]" value="" tabindex="1" readonly="">
            <i data-icon="depart2"></i>
        </div>
        <div class="search__field col col--search calendar calendar__arrival" data-name="depart-date" data-next="">
            <input  data-selector="pickmeup" data-type="arrival" data-min="" type="text" placeholder="Обратно.." class="input date__to input--search input--dropdown" name="arrival_date[]" value="" tabindex="1" readonly="">
            <i data-icon="depart2"></i>
            <input type="hidden" name="utc_arrival_date[]" value="">
        </div>
        <div class="search__field col col--search search__field-dropdown" data-name="travelers">
            <input id="travelers" data-traveler="traveler" type="text" placeholder="Travelers.." class="input input--search input--dropdown" value="">
            <i data-icon="travelers"></i>
            <label for="travelers" class="travelers__label">
                <span data-name="title" id="travelers__number">1</span>
                <span data-name="title" class="form-input__choose">Эконом</span>
            </label>
            <div class="dropdown">
                <div class="dd__row" data-group="category">
                    <label>
                        <!--div class="selectric-wrapper">
                            <div class="selectric">
                                <p class="label selectric__choose">Economy</p><b class="button">▾</b>
                            </div>
                            <div class="selectric-items">
                                <div class="selectric-scroll">
                                    <ul>
                                        <li data-index="Y" class="selected">Эконом-класс</li>
                                        <li data-index="C" class="">Бизнес-класс</li>
                                    </ul>
                                </div>
                            </div><input class="selectric-input" tabindex="Y">
                        </div-->
                        <select class="makeSelectric" name="class">
                            <option value="Y" selected>Эконом-класс</option>
                            <option value="C">Бизнес-класс</option>
                        </select>
                    </label>
                </div>
                <div class="dd__row" data-group="travelers">
                    <div class="dd__group row--travelers dd__group-adults">
                        <label><span>Adults</span>
                            <input type="text" name="adults[]" id="adults" value="1" class="dd__row-input">
                        </label>
                        <button data-type="increase" type="button" class="button--val">+</button>
                        <button data-type="decrease" type="button" class="button--val">-</button>
                    </div>
                    <div class="dd__group row--travelers dd__group-children">
                        <label><span>Children</span>
                            <input type="text" name="children[]" id="children" value="0" class="dd__row-input">
                        </label>
                        <button data-type="increase" type="button" class="button--val">+</button>
                        <button data-type="decrease" type="button" class="button--val">-</button>
                    </div>
                    <div class="dd__group row--travelers dd__group-infants">
                        <label><span>Infants</span>
                            <input type="text" name="infants[]" id="infants" value="0" class="dd__row-input">
                        </label>
                        <button data-type="increase" type="button" class="button--val">+</button>
                        <button data-type="decrease" type="button" class="button--val">-</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="search__action col col--search">
            <button type="submit" class="button" data-icon="search">Найти</button>
        </div>
        <div class="search__options">
            <div data-col="">
                <input id="direct-flight" type="checkbox" checked="checked" name="direct_flights[]" value="1">
                <label for="direct-flight">Только прямые рейсы</label>
                <i data-icon="verify"></i>
            </div>
        </div>
    </fieldset>
</div>
{{ Form::close() }}