@extends ('layout')

@section('header')
<div class="modals">
    <div class="header__search">
        <div class="wrapper wrapper--search">

            @include('flights.index')

            <form class="form--search form-search__hotel" action="" method="post" data-name="hotel">
                <fieldset>

                    <div class="search__field search__field__typeahead col col--search" data-name="destination" data-next="">
                        <span class="twitter-typeahead">
                            <input data-target="find_hotel" placeholder="Find hotel in.." type="text" value="" class="input input--search input--dropdown tt-input autocomplete__input" data-selector="find_hotel" autocomplete="off">
                        </span>
                        <i data-icon="location"></i>
                    </div>

                    <div class="search__field col col--search calendar" data-name="depart-date" data-next="">
                        <input  data-selector="pickmeup" data-type="departure" data-min="" type="text" placeholder="Check-in.." class="input input--search input--dropdown date__from" name="departure_date[]"  autocomplete="off">
                        <i data-icon="depart2"></i>
                    </div>

                    <div class="search__field col col--search calendar" data-name="depart-date" data-next="">
                        <input data-selector="pickmeup" data-type="arrival" data-min="" type="text" placeholder="Check-out.." class="input input--search input--dropdown date__to" name="arrival_date[]" value="" tabindex="1" readonly=""  autocomplete="off">
                        <i data-icon="depart2"></i>
                    </div>

                    <div class="search__field col col--search search__field-dropdown" id="hotel__room" data-name="rooms">
                        <input id="rooms" data-traveler="traveler" type="text" placeholder="Rooms.." class="input input--search input--dropdown" readonly="" value="" tabindex="1">
                        <i data-icon="hotel"></i>
                        <label for="rooms" class="travelers__label">
                            <span id="rooms__number">1 Room</span>
                        </label>
                        <div class="dropdown">
                            <div class="dd__row" data-group="rooms">
                                <label>
                                    <div class="selectric-wrapper">
                                        <div class="selectric">
                                            <p class="label selectric__choose">1 Room</p><b class="button">▾</b>
                                        </div>
                                        <div class="selectric-items" tabindex="-1">
                                            <div class="selectric-scroll" id="selectric-scroll__rooms">
                                                <ul>
                                                    <li data-index="1" class="selected">1 Room</li>
                                                    <li data-index="2" class="">2 Rooms</li>
                                                    <li data-index="3" class="">3 Rooms</li>
                                                    <li data-index="4" class="">4 Rooms</li>
                                                    <li data-index="5" class="">5 Rooms</li>
                                                    <li data-index="6" class="last">6 Rooms</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div class="dd__row" data-group="travelers">

                                <div class="has--panels tabs__room" data-group="tabs">
                                    <ul class="tabs__caption-room" data-list="tab-links" role="tablist">
                                        <li class="tabs__room-items" role="tab" aria-selected="true" aria-controls="tab--A" rel="tab">
                                            <a href="#tab--A"><span>Room A</span></a>
                                        </li>
                                        <li class="tabs__room-items" role="tab" aria-selected="true" aria-controls="tab--B">
                                            <a href="#tab--B"><span>Room B</span></a>
                                        </li>
                                        <li class="tabs__room-items" role="tab" aria-selected="true" aria-controls="tab--C">
                                            <a href="#tab--C"><span>Room C</span></a>
                                        </li>
                                        <li class="tabs__room-items" role="tab" aria-selected="true" aria-controls="tab--D">
                                            <a href="#tab--D"> <span>Room D</span></a>
                                        </li>
                                        <li class="tabs__room-items" role="tab" aria-selected="true" aria-controls="tab--E">
                                            <a href="#tab--E"> <span>Room E</span></a>
                                        </li>
                                        <li class="tabs__room-items" role="tab" aria-selected="true" aria-controls="tab--F">
                                            <a href="#tab--F"><span>Room F</span></a>
                                        </li>
                                    </ul>
                                    <div data-group="tab-panels">
                                        <div id="tab--A" class="panel tabs__content" role="tabpanel" aria-hidden="false" data-index="0">
                                            <div class="dd__group">
                                                <label>
                                                    <button data-type="increase" type="button" class="button--val">+</button>
                                                    <input type="text" data-name="adults" name="rooms[0][adults]" id="adults" value="1" maxlength="1">
                                                    <button data-type="decrease" type="button" class="button--val">-</button>
                                                </label>
                                                <span data-name="input-title">Adults (18+)</span>
                                            </div>
                                            <div class="dd__group">
                                                <label for="children">
                                                    <button data-type="increase" type="button" class="button--val">+</button>
                                                    <input type="text" data-name="children" name="rooms[0][children]" maxlength="1" value="0">
                                                    <button data-type="decrease" type="button" class="button--val">-</button>
                                                </label>
                                                <span data-name="input-title">Children (2-17)</span>
                                            </div>

                                            <div class="dd__group select__child-age" data-name="select-age">
                                                <label>
                                                    <div class="selectric-wrapper">
                                                        <div class="selectric">
                                                            <p class="label selectric__child-age">6</p>
                                                            <b class="button">▾</b>
                                                        </div>
                                                        <div class="selectric-items" tabindex="-1">
                                                            <div class="selectric-scroll">
                                                                <ul>
                                                                    <li data-index="1" class="">1</li>
                                                                    <li data-index="2" class="">2</li>
                                                                    <li data-index="3" class="">3</li>
                                                                    <li data-index="4" class="">4</li>
                                                                    <li data-index="5" class="">5</li>
                                                                    <li data-index="6" class="selected">6</li>
                                                                    <li data-index="7" class="">7</li>
                                                                    <li data-index="8" class="">8</li>
                                                                    <li data-index="9" class="">9</li>
                                                                    <li data-index="10" class="">10</li>
                                                                    <li data-index="11" class="">11</li>
                                                                    <li data-index="12" class="">12</li>
                                                                    <li data-index="13" class="">13</li>
                                                                    <li data-index="14" class="">14</li>
                                                                    <li data-index="15" class="">15</li>
                                                                    <li data-index="16" class="">16</li>
                                                                    <li data-index="17" class="last">17</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </label>
                                                <span data-name="input-title">Child Age A</span>
                                            </div>

                                            <div class="dd__group select__child-age" data-name="select-age">
                                                <label>
                                                    <div class="selectric-wrapper">
                                                        <div class="selectric">
                                                            <p class="label selectric__child-age">6</p><b class="button">▾</b>
                                                        </div>
                                                        <div class="selectric-items" tabindex="-1">
                                                            <div class="selectric-scroll">
                                                                <ul>
                                                                    <li data-index="1" class="">1</li>
                                                                    <li data-index="2" class="">2</li>
                                                                    <li data-index="3" class="">3</li>
                                                                    <li data-index="4" class="">4</li>
                                                                    <li data-index="5" class="">5</li>
                                                                    <li data-index="6" class="selected">6</li>
                                                                    <li data-index="7" class="">7</li>
                                                                    <li data-index="8" class="">8</li>
                                                                    <li data-index="9" class="">9</li>
                                                                    <li data-index="10" class="">10</li>
                                                                    <li data-index="11" class="">11</li>
                                                                    <li data-index="12" class="">12</li>
                                                                    <li data-index="13" class="">13</li>
                                                                    <li data-index="14" class="">14</li>
                                                                    <li data-index="15" class="">15</li>
                                                                    <li data-index="16" class="">16</li>
                                                                    <li data-index="17" class="last">17</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </label>
                                                <span data-name="input-title">Child Age B</span>
                                            </div>

                                            <div class="dd__group select__child-age" data-name="select-age">
                                                <label>
                                                    <div class="selectric-wrapper">
                                                        <div class="selectric">
                                                            <p class="label selectric__child-age">6</p><b class="button">▾</b>
                                                        </div>
                                                        <div class="selectric-items" tabindex="-1">
                                                            <div class="selectric-scroll">
                                                                <ul>
                                                                    <li data-index="1" class="">1</li>
                                                                    <li data-index="2" class="">2</li>
                                                                    <li data-index="3" class="">3</li>
                                                                    <li data-index="4" class="">4</li>
                                                                    <li data-index="5" class="">5</li>
                                                                    <li data-index="6" class="selected">6</li>
                                                                    <li data-index="7" class="">7</li>
                                                                    <li data-index="8" class="">8</li>
                                                                    <li data-index="9" class="">9</li>
                                                                    <li data-index="10" class="">10</li>
                                                                    <li data-index="11" class="">11</li>
                                                                    <li data-index="12" class="">12</li>
                                                                    <li data-index="13" class="">13</li>
                                                                    <li data-index="14" class="">14</li>
                                                                    <li data-index="15" class="">15</li>
                                                                    <li data-index="16" class="">16</li>
                                                                    <li data-index="17" class="last">17</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </label>
                                                <span data-name="input-title">Child Age C</span>
                                            </div>

                                            <div class="dd__group select__child-age" data-name="select-age">
                                                <label>
                                                    <div class="selectric-wrapper">
                                                        <div class="selectric">
                                                            <p class="label selectric__child-age">6</p><b class="button">▾</b>
                                                        </div>
                                                        <div class="selectric-items" tabindex="-1">
                                                            <div class="selectric-scroll">
                                                                <ul>
                                                                    <li data-index="1" class="">1</li>
                                                                    <li data-index="2" class="">2</li>
                                                                    <li data-index="3" class="">3</li>
                                                                    <li data-index="4" class="">4</li>
                                                                    <li data-index="5" class="">5</li>
                                                                    <li data-index="6" class="selected">6</li>
                                                                    <li data-index="7" class="">7</li>
                                                                    <li data-index="8" class="">8</li>
                                                                    <li data-index="9" class="">9</li>
                                                                    <li data-index="10" class="">10</li>
                                                                    <li data-index="11" class="">11</li>
                                                                    <li data-index="12" class="">12</li>
                                                                    <li data-index="13" class="">13</li>
                                                                    <li data-index="14" class="">14</li>
                                                                    <li data-index="15" class="">15</li>
                                                                    <li data-index="16" class="">16</li>
                                                                    <li data-index="17" class="last">17</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </label>
                                                <span data-name="input-title">Child Age D</span>
                                            </div>

                                            <div class="dd__group select__child-age" data-name="select-age">
                                                <label>
                                                    <div class="selectric-wrapper">
                                                        <div class="selectric">
                                                            <p class="label selectric__child-age">6</p>
                                                            <b class="button">▾</b>
                                                        </div>
                                                        <div class="selectric-items" tabindex="-1">
                                                            <div class="selectric-scroll">
                                                                <ul>
                                                                    <li data-index="1" class="">1</li>
                                                                    <li data-index="2" class="">2</li>
                                                                    <li data-index="3" class="">3</li>
                                                                    <li data-index="4" class="">4</li>
                                                                    <li data-index="5" class="">5</li>
                                                                    <li data-index="6" class="selected">6</li>
                                                                    <li data-index="7" class="">7</li>
                                                                    <li data-index="8" class="">8</li>
                                                                    <li data-index="9" class="">9</li>
                                                                    <li data-index="10" class="">10</li>
                                                                    <li data-index="11" class="">11</li>
                                                                    <li data-index="12" class="">12</li>
                                                                    <li data-index="13" class="">13</li>
                                                                    <li data-index="14" class="">14</li>
                                                                    <li data-index="15" class="">15</li>
                                                                    <li data-index="16" class="">16</li>
                                                                    <li data-index="17" class="last">17</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </label>
                                                <span data-name="input-title">Child Age E</span>
                                            </div>

                                            <div class="dd__group select__child-age" data-name="select-age">
                                                <label>
                                                    <div class="selectric-wrapper">
                                                        <div class="selectric">
                                                            <p class="label selectric__child-age">6</p>
                                                            <b class="button">▾</b>
                                                        </div>
                                                        <div class="selectric-items" tabindex="-1">
                                                            <div class="selectric-scroll">
                                                                <ul>
                                                                    <li data-index="1" class="">1</li>
                                                                    <li data-index="2" class="">2</li>
                                                                    <li data-index="3" class="">3</li>
                                                                    <li data-index="4" class="">4</li>
                                                                    <li data-index="5" class="">5</li>
                                                                    <li data-index="6" class="selected">6</li>
                                                                    <li data-index="7" class="">7</li>
                                                                    <li data-index="8" class="">8</li>
                                                                    <li data-index="9" class="">9</li>
                                                                    <li data-index="10" class="">10</li>
                                                                    <li data-index="11" class="">11</li>
                                                                    <li data-index="12" class="">12</li>
                                                                    <li data-index="13" class="">13</li>
                                                                    <li data-index="14" class="">14</li>
                                                                    <li data-index="15" class="">15</li>
                                                                    <li data-index="16" class="">16</li>
                                                                    <li data-index="17" class="last">17</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </label>
                                                <span data-name="input-title">Child Age F</span>
                                            </div>

                                        </div>
                                        <div id="tab--B" class="panel tabs__content" role="tabpanel" aria-hidden="false" data-index="0">
                                            <div class="dd__group">
                                                <label>
                                                    <button data-type="increase" type="button" class="button--val">+</button>
                                                    <input type="text" data-name="adults" name="" id="adults" value="1" maxlength="1">
                                                    <button data-type="decrease" type="button" class="button--val">-</button>
                                                </label>
                                                <span data-name="input-title">Adults (18+)</span>
                                            </div>
                                            <div class="dd__group">
                                                <label for="children">
                                                    <button data-type="increase" type="button" class="button--val">+</button>
                                                    <input type="text" data-name="children" name="" id="children" maxlength="1" value="0">
                                                    <button data-type="decrease" type="button" class="button--val">-</button>
                                                </label>
                                                <span data-name="input-title">Children (2-17)</span>
                                            </div>
                                        </div>
                                        <div id="tab--C" class="panel tabs__content" role="tabpanel" aria-hidden="false" data-index="0">
                                            <div class="dd__group">
                                                <label>
                                                    <button data-type="increase" type="button" class="button--val">+</button>
                                                    <input type="text" data-name="adults" name="" id="adults" value="1" maxlength="1">
                                                    <button data-type="decrease" type="button" class="button--val">-</button>
                                                </label>
                                                <span data-name="input-title">Adults (18+)</span>
                                            </div>
                                            <div class="dd__group">
                                                <label for="children">
                                                    <button data-type="increase" type="button" class="button--val">+</button>
                                                    <input type="text" data-name="children" name="" id="children" maxlength="1" value="0">
                                                    <button data-type="decrease" type="button" class="button--val">-</button>
                                                </label>
                                                <span data-name="input-title">Children (2-17)</span>
                                            </div>
                                        </div>
                                        <div id="tab--D" class="panel tabs__content" role="tabpanel" aria-hidden="false" data-index="0">
                                            <div class="dd__group">
                                                <label>
                                                    <button data-type="increase" type="button" class="button--val">+</button>
                                                    <input type="text" data-name="adults" name="" id="adults" value="1" maxlength="1">
                                                    <button data-type="decrease" type="button" class="button--val">-</button>
                                                </label>
                                                <span data-name="input-title">Adults (18+)</span>
                                            </div>
                                            <div class="dd__group">
                                                <label for="children">
                                                    <button data-type="increase" type="button" class="button--val">+</button>
                                                    <input type="text" data-name="children" name="" id="children" maxlength="1" value="0">
                                                    <button data-type="decrease" type="button" class="button--val">-</button>
                                                </label>
                                                <span data-name="input-title">Children (2-17)</span>
                                            </div>
                                        </div>
                                        <div id="tab--E" class="panel tabs__content" role="tabpanel" aria-hidden="false" data-index="0">
                                            <div class="dd__group">
                                                <label>
                                                    <button data-type="increase" type="button" class="button--val">+</button>
                                                    <input type="text" data-name="adults" name="rooms[0][adults]" id="adults" value="1" maxlength="1">
                                                    <button data-type="decrease" type="button" class="button--val">-</button>
                                                </label>
                                                <span data-name="input-title">Adults (18+)</span>
                                            </div>
                                            <div class="dd__group">
                                                <label for="children">
                                                    <button data-type="increase" type="button" class="button--val">+</button>
                                                    <input type="text" data-name="children" name="" id="children" maxlength="1" value="0">
                                                    <button data-type="decrease" type="button" class="button--val">-</button>
                                                </label>
                                                <span data-name="input-title">Children (2-17)</span>
                                            </div>
                                        </div>
                                        <div id="tab--F" class="panel tabs__content" role="tabpanel" aria-hidden="false" data-index="0">
                                            <div class="dd__group">
                                                <label>
                                                    <button data-type="increase" type="button" class="button--val">+</button>
                                                    <input type="text" data-name="adults" name="" id="adults" value="1" maxlength="1">
                                                    <button data-type="decrease" type="button" class="button--val">-</button>
                                                </label>
                                                <span data-name="input-title">Adults (18+)</span>
                                            </div>
                                            <div class="dd__group">
                                                <label for="children">
                                                    <button data-type="increase" type="button" class="button--val">+</button>
                                                    <input type="text" data-name="children" name="" id="children" maxlength="1" value="0">
                                                    <button data-type="decrease" type="button" class="button--val">-</button>
                                                </label>
                                                <span data-name="input-title">Children (2-17)</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="search__action col col--search">
                        <button type="submit" class="button" data-icon="search" tabindex="1">Search</button>
                    </div>
                </fieldset>
            </form>

            <form class="form--search form-search__car" action="" method="post" data-name="car">
                <fieldset>

                    <div class="search__field search__field__typeahead col col--search active" data-name="destination" data-next="">
                        <span class="twitter-typeahead">
                            <input placeholder="Find car in.." type="text" value="" class="input input--search input--dropdown tt-input autocomplete__input" data-selector="find-car">
                        </span>
                        <i data-icon="location"></i>
                    </div>

                    <div class="search__field col col--search calendar" data-name="depart-date" data-next="">
                        <input data-selector="pickmeup" data-type="departure" data-min="" type="text" placeholder="Check-in.." class="input input--search input--dropdown date__from" name="departure_date[]" value="" tabindex="1" readonly="">
                        <i data-icon="depart2"></i>
                    </div>

                    <div class="search__field col col--search calendar" data-name="depart-date" data-next="">
                        <input  data-selector="pickmeup" data-type="arrival" data-min="" type="text" placeholder="Check-out.." class="input input--search input--dropdown date__to" name="arrival_date[]" value="" tabindex="1" readonly="">
                        <i data-icon="depart2"></i>
                    </div>

                    <div class="search__field col col--search search__field-dropdown" data-name="rooms">
                        <input id="cars" data-traveler="traveler" type="text" placeholder="Car.." class="input input--search input--dropdown">
                        <i data-icon="car"></i>
                        <label for="cars" class="travelers__label">
                            <span class="form-input__choose">No preference</span>
                        </label>
                        <div class="dropdown">
                            <div class="dd__row" data-group="rooms">
                                <label>
                                    <div class="selectric-wrapper">
                                        <div class="selectric">
                                            <p class="label selectric__choose">No preference</p><b class="button">▾</b>
                                        </div>
                                        <div class="selectric-items">
                                            <div class="selectric-scroll">
                                                <ul>
                                                    <li data-index="0" class="selected">No preference</li>
                                                    <li data-index="1" class="">Economy</li>
                                                    <li data-index="2" class="">Compact</li>
                                                    <li data-index="3" class="">Midsize</li>
                                                    <li data-index="4" class="">Standard</li>
                                                    <li data-index="5" class="">Fullsize</li>
                                                    <li data-index="6" class="">Premium</li>
                                                    <li data-index="7" class="">Luxury</li>
                                                    <li data-index="8" class="">Convertible</li>
                                                    <li data-index="9" class="">Minivan</li>
                                                    <li data-index="10" class="">Sport Utility Vehicles</li>
                                                    <li data-index="11" class="last">Sports car</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="search__action col col--search">
                        <button type="submit" class="button" data-icon="search" tabindex="1">Search</button>
                    </div>
                </fieldset>
            </form>

            <form class="form--search form-search__pack" action="" method="post" data-name="pack"></form>

            @include('insurance.index')
        </div>
    </div>
</div>
<script>
    $('#insuranceSearchForm').submit(function () {
        $(this).find(".selectric").each(function () {
            $('#insuranceSearchForm').append(
                    $("<input>", {
                        name: 'age[]',
                        value: $(this).find(".selectric__choose").html(),
                        type: 'hidden'
                    })
                    );
        });
    });
    $(".flightsCitiesList").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/autocomplete/cities",
                dataType: "json",
                data: {
                    term: request.term
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 2
    });
</script>
@endsection