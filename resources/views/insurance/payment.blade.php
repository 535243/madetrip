@extends ('layout')

@section('content')


    <div class="row " style="margin-top: 72px; margin-bottom: 50px;">
        <div class="col-md-6 offset-md-3">
            <form method="post" action="https://pay.modulbank.ru/pay" id="payment">

                <input type="hidden" name="testing" value="{{$paymentData['testing']}}">

                <input type="hidden" name="salt" value="{{$paymentData['salt']}}">

                <input type="hidden" name="order_id" value="{{$paymentData['order_id']}}">

                <input type="hidden" name="amount" value="{{$paymentData['amount']}}">

                <input type="hidden" name="merchant" value="{{$paymentData['merchant']}}">

                <input type="hidden" name="signature" value="{{$paymentData['signature']}}">

                <input type="hidden" name="unix_timestamp" value="{{$paymentData['unix_timestamp']}}">

                <input type="hidden" name="description" value="{{$paymentData['description']}}">

                <!--input type="hidden" name="client_email" value="test@test.ru"-->

                <input type="hidden" name="success_url" value="{{$paymentData['success_url']}}">

                <input type="hidden" name="fail_url" value="{{$paymentData['fail_url']}}">
            </form>
            <h3>Подтверждение оплаты</h3>
            <p><b>Номер заказа:</b> {{$order_id}}</p>
            <p><b>Сумма оплаты:</b> {{$price}} руб</p>
            <hr>
            <h4 style="margin-bottom: 20px">Перейти к оплате</h4>
            <button onclick="$('form#payment').submit();" class="btn btn-warning btn-lg"><img style="width: 20px" src="{{asset('icons/credit-card.svg')}}"
                > К оплате</button>
        </div>
    </div>


</form>