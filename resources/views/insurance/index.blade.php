{{ Form::open(array('url' => '/findInsurance', 'method' => 'post', 'class' => 'form--search form-search__Insurance', 'id' => 'insuranceSearchForm', 'data-name' => 'car')) }}
<fieldset>
    <div class="search__field search__field__typeahead col col--search active" data-name="destination" data-next="">
        <span class="twitter-typeahead">
            <div class="ui-widget">
                <!--input placeholder="Название страны..." type="text" value="" class="input input--search input--dropdown tt-input autocomplete__input" data-selector="find-car"-->

                <select id="combobox">
                    @foreach($countryList as $key=>$value)
                    <option value="{{$key}}" {{ ($key == "Worldwide" ? "selected" : "") }}>{{isset($value['name_rus']) ? $value['name_rus'] : $value['Name']}}</option>
                    @endforeach
                </select>
            </div>
        </span>
        <i data-icon="location"></i>
    </div>

    <div class="search__field col col--search calendar" data-name="depart-date" data-next="">
        <input data-selector="pickmeup" data-type="departure" data-min="" type="text" placeholder="Туда.." class="input input--search input--dropdown date__from"  name="date_from" value="" tabindex="1" readonly="">
        <i data-icon="depart2"></i>
    </div>

    <div class="search__field col col--search calendar" data-name="depart-date" data-next="">
        <input  data-selector="pickmeup" data-type="arrival" data-min="" type="text" placeholder="Обратно.." class="input input--search input--dropdown date__to" name="date_to" value="" tabindex="1" readonly="">
        <i data-icon="depart2"></i>
    </div>

    <div class="search__field col col--search search__field-dropdown" data-name="rooms">
        <input id="cars" data-traveler="traveler" type="text" placeholder="Car.." class="input input--search input--dropdown">
        <i data-icon="travelers"></i>
        <label for="travelers" class="travelers__label">
            <span id="insur__number">1</span>-й
            <span class="insur__title">человек</span>
        </label>
        <div class="dropdown insur__dropdown">
            <div class="dd__group calc__insur">
                <label for="children">
                    <button data-type="increase" type="button" class="button--val">+</button>
                    <input type="text" value="1" class="insur__row-input">
                    <button data-type="decrease" type="button" class="button--val">-</button>
                </label>
                <span data-name="input-title">Количество</span>
            </div>
            <div class="insur__age-list">
                <div class="dd__row insur__age" data-group="rooms">
                    <label>
                        <span data-name="input-title" class="d-inline-block">Возраст туриста</span> 
                        <input type="text" name="age[]" placeholder="30" class="form-control rounded-0 w-25 d-inline-block">    
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="search__action col col--search">
        <button type="submit" class="button"> <!--data-icon="search"-->Расчет стоимости</button>
    </div>
</fieldset>
{{ Form::close() }}