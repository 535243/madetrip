@extends ('layout')

@section('content')

    <div class="row " style="margin-top: 72px; margin-bottom: 50px;">
        <div class="col-sm-6 text-right" style="padding-top: 65px;">
            <h2>Оформление страхования</h2>
            <p class="lead">Пожалуйста, заполните поля формы</p>
        </div>
        <div class="col-sm-6">
            <img src="{{asset('images/Rosgosstrah_logo.png')}}" style="width: 70%; margin: auto">
        </div>
    </div>
    {{ Form::open(array('url' => '/payment', 'method' => 'post', 'id' => 'checkout')) }}
    <input type="hidden" name="apiID" value="{{$policyInfo['apiID']}}" />
    <div class="row">
        <div class="col-md-12">
            <h4 style="margin-bottom: 20px">Застрахованные</h4>
            @foreach ($policyInfo['insured'] as $i=>$insured)
                <div class="row personInfo">
                        <div class="form-group col-md-1">
                            <img src="{{asset('icons/person.svg')}}" style="width: 32px; @if ($i == 0) margin-top: 34px; @endif" />
                        </div>
                        <div class="form-group col-md-4">
                            @if ($i == 0)
                                <label>Имя и фамилия</label>
                            @endif
                            <input type="text" class="form-control personName" placeholder="Ivan Ivanov" name="name[{{$i}}]" required>
                        </div>
                        <div class="form-group col-md-3">
                            @if ($i == 0)
                                <label>Дата рождения</label>
                            @endif
                            <input class="datepicker form-control personBirthday" name="birthday[{{$i}}]" type="text" placeholder="00-00-0000" required>
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                        <div class="form-group col-md-3">
                            @if ($i == 0)
                                <label>Является страхователем</label>
                            @endif
                            <input type="checkbox" class="form-control" name="isInsurer[{{$i}}]" id="isInsurer-{{$i}}">
                        </div>
                </div>
            @endforeach
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h4 style="margin-bottom: 20px">Страхователь</h4>
            <div class="row">
                <div class="form-group col-md-1">
                    <img src="{{asset('icons/person.svg')}}" style="width: 32px; " />
                </div>
                <div class="form-group col-md-4">
                    <input type="text" class="form-control" placeholder="Ivan Ivanov" name="insurerName" required>
                </div>
                <div class="form-group col-md-3">
                    <input class="datepicker form-control" name="insurerBirthday" type="text" required>
                    <span class="add-on"><i class="icon-th"></i></span>
                </div>
                <div class="form-group col-md-3">
                    <input class="form-control" name="insurerEmail" type="email" placeholder="E-mail адрес" required>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <h4 style="margin-bottom: 20px">Информация о поездке:</h4>
            <ul class="list-group">
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Страны поздки:
                    <div style="display: inline;">
                        @foreach($policyInfo['countries'] as $country)
                        <span class="badge badge-primary badge-pill">{{$country}}</span>
                        @endforeach
                    </div>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Даты поездки
                    <div style="display: inline">
                        <span class="badge badge-primary badge-pill">{{$policyInfo['dates'][0]}}</span>
                        <span class="badge badge-primary badge-pill">{{$policyInfo['dates'][1]}}</span>
                    </div>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Стоимость страхового полиса
                    <span class="badge badge-warning badge-pill" style="font-size: 20px;">{{$policyInfo['price']}} руб</span>
                </li>
            </ul>
        </div>
        <div class="col-md-6">
            <h4 style="margin-bottom: 20px">Перейти к оплате</h4>
            <button id="paymentButton" class="btn btn-warning btn-lg"><img style="width: 20px" src="{{asset('icons/credit-card.svg')}}"
                > К оплате</button>
        </div>
    </div>
    {{ Form::close() }}


    <script>
        $(document).ready( function () {
            $(".datepicker").datepicker({
                format: "dd-mm-yyyy"
            });
            $("input[type=checkbox]").click(function () {
                let that = $(this);
                $(this).closest('form').find('input[type=checkbox]').each(function () {
                    if ($(this).attr('id') != that.attr('id')) {
                        $(this)[0].checked = false;
                    }
                });
            });
            $("input[type=checkbox]").click(function () {
                if ($(this)[0].checked) {
                    let insurerName = $(this).closest('.personInfo').find('.personName').val();
                    let insurerBirthday = $(this).closest('.personInfo').find('.personBirthday').val();
                    $('input[name=insurerName]').val( insurerName );
                    $('input[name=insurerBirthday]').val( insurerBirthday );
                }
                else {
                    $('input[name=insurerName]').val('');
                    $('input[name=insurerBirthday]').val('');
                }
            });
        });
    </script>
@endsection