@extends ('layout')

@section('content')

    @if ($success)
    <div class="row " style="margin-top: 72px; margin-bottom: 50px;">
        <div class="col-sm-6 text-right" style="padding-right: 50px">
            <img src="{{asset('icons/success.svg')}}" style="width: 50%; margin: auto">
        </div>
        <div class="col-sm-6 text-left" style="padding-top: 65px;">
            <h2>Успех!</h2>
            <p class="lead">Полис был отправлен на вашу электронную почту</p>

            <p style="font-size: 12px">
                Если вы <span class="text-danger">не получили полис</span> на свою электронную почту, <a href="mailto:admin@example.com">напишите нам</a> об этом
            </p>
        </div>
    </div>
    @else
    <div class="row " style="margin-top: 72px; margin-bottom: 50px;">
        <div class="col-sm-6 text-right" style="padding-right: 50px">
            <img src="{{asset('icons/error.svg')}}" style="width: 50%; margin: auto">
        </div>
        <div class="col-sm-6 text-left" style="padding-top: 65px;">
            <h2>Ошибка!</h2>
            <p class="lead">При акцептации полиса возникли следующие ошибки:</p>

            <div class="card text-white bg-danger mb-3">
                <div class="card-body">
                    <p class="card-text">{{$errors[0]}}</p>
                </div>
            </div>

            <p style="font-size: 12px">
                Для получения полиса, <a href="mailto:admin@example.com">напишите нам</a>, и мы решим Вашу проблему
            </p>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-4 offset-md-4" style="margin-top: 100px">
            <a href="/" class="btn btn-warning btn-block" style="font-size: 22px; line-height: 2;"><img src="{{asset('icons/home.svg')}}" style="width: 32px" /> На главную</a>
        </div>
    </div>
@endsection