@extends ('results')

@section('content')
<div class="result__page">
    <div class="container">
        <div class="filter-nav d-none">
            <div class="filter-nav__inner">
                <ul class="filter-nav__items">
                    <li class="filter-nav__placeholder">
                        <span class="filter-nav__link"><i class="fas fa-suitcase"></i></span>
                    </li>
                    <li class="filter-nav__item">
                        <a href="#" class="filter-nav__link active"><i class="fas fa-suitcase"></i></a>
                    </li>
                    <li class="filter-nav__item">
                        <a class="filter-nav__link text-muted"><i class="fas fa-plane"></i></a>
                    </li>
                    <li class="filter-nav__item">
                        <a class="filter-nav__link text-muted"><i class="fas fa-bed"></i></a>
                    </li>
                    <li class="filter-nav__item">
                        <a class="filter-nav__link text-muted"><i class="fas fa-car"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="filter-trigger"></div>
        <div class="result__main mt-3">           
            <div class="result-insurance drop-shadow rounded-right rounded-left ">
                @if ($results)
                @foreach($results as $result)
                <div class="card w-100 mb-3">
                    <div class="card-body w-100">
                        {{ Form::open(array('url' => '/checkout', 'method' => 'post')) }}
                        <div class="row">
                            <div class="col-md-3">
                                <img src="/images/{{ $result['apiID'] }}.png" />
                            </div>
                            <div class="col-md-6 text-center">
                                <button type="button" class="btn result-insurance__btn btn-sm">
                                    <i class="fas fa-arrow-down"></i> подробнее
                                </button>
                            </div>
                            <div class="col-md-3 text-center">
                                <div class="insurance-price mb-2">{{$result['price']}} руб</div>
                                <button type="submit" class="btn btn-success">
                                    <i class="fas fa-cart-plus"></i> Купить
                                </button>
                            </div>
                            <input type="hidden" name="apiID" value="{{ $result['apiID'] }}"/>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
            <div class="insurance-filter">
                <span class="filter-close"></span>
                <div class="insurance-filter__inner">
                    <div class="insurance-filter__block filters-county"><i class="fas fa-map-marker-alt"></i>
                        <div class="insurance-filter__title filters-county__title closed d-inline-block">
                            @if((isset($policy->countries))) {{$policy->countries}} @else Не указаны страны @endif 
                        </div>
                        <select class="multi__select-country" multiple="multiple">
                            <option>Италия</option>
                            <option>Черногория</option>
                            <option>Польша</option>
                        </select>
                    </div>

                    <div class="insurance-filter__block filters-period">
                        <i class="far fa-calendar-alt"></i>
                        <div class="insurance-filter__title filters-period__title closed d-inline-block"> @if((isset($policy->date_from))) {{$policy->date_from}} &mdash; {{$policy->date_to}} @else Дата не указана @endif</div>
                        <div class="filters-period__calendar">
                            <input class="filters-period__input filters-period__from" placeholder="Туда..">
                            <input class="filters-period__input filters-period__to" placeholder="Обратно...">
                        </div>
                    </div>
                    <div class="insurance-filter__block filter-travelers">
                        <i class="fas fa-user-alt"></i>
                        <div class="insurance-filter__title filter-travelers__title closed d-inline-block">
                            @if(isset($policy->insured)) {{$policy->insured}} чел. @else<span id="number-of-travelers">1</span>
                            путешественник @endif
                        </div>
                        <ul class="travelers__items input__items mCustomScrollbar" data-mcs-theme="dark">
                            <li class="travelers__item input__item">
                                <div class="travelers__item-number">
                                    <label class="filter-input__label checkbox-label">
                                        <input type="checkbox" class="filter-checkbox get-select__input" checked>
                                        <div class="label-text checkbox-label">1 <sup>й</sup> путешественник</div>
                                    </label>
                                </div>
                                <div class="sup-options">
                                    <div class="travelers__age-title">Возраст</div>
                                    <div class="travelers__age-select">
                                        <select class="travelers__age">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                        </select>
                                    </div>
                                </div>
                            </li>
                            <li class="travelers__item input__item">
                                <div class="travelers__item-number">
                                    <label class="filter-input__label checkbox-label">
                                        <input type="checkbox" class="filter-checkbox get-select__input">
                                        <div class="label-text checkbox-label">2 <sup>й</sup> путешественник</div>
                                    </label>
                                </div>
                                <div class="sup-options">
                                    <div class="travelers__age-title">Возраст</div>
                                    <div class="travelers__age-select">
                                        <select class="travelers__age">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                        </select>
                                    </div>
                                </div>
                            </li>
                            <li class="travelers__item input__item">
                                <div class="travelers__item-number">
                                    <label class="filter-input__label checkbox-label">
                                        <input type="checkbox" class="filter-checkbox get-select__input">
                                        <div class="label-text checkbox-label">3 <sup>й</sup> путешественник</div>
                                    </label>
                                </div>
                                <div class="sup-options">
                                    <div class="travelers__age-title">Возраст</div>
                                    <div class="travelers__age-select">
                                        <select class="travelers__age">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                        </select>
                                    </div>
                                </div>
                            </li>
                            <li class="travelers__item input__item">
                                <div class="travelers__item-number">
                                    <label class="filter-input__label checkbox-label">
                                        <input type="checkbox" class="filter-checkbox get-select__input">
                                        <div class="label-text checkbox-label">4 <sup>й</sup> путешественник</div>
                                    </label>
                                </div>
                                <div class="sup-options">
                                    <div class="travelers__age-title">Возраст</div>
                                    <div class="travelers__age-select">
                                        <select class="travelers__age">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                        </select>
                                    </div>
                                </div>
                            </li>
                            <li class="travelers__item input__item">
                                <div class="travelers__item-number">
                                    <label class="filter-input__label checkbox-label">
                                        <input type="checkbox" class="filter-checkbox get-select__input">
                                        <div class="label-text checkbox-label">5 <sup>й</sup> путешественник</div>
                                    </label>
                                </div>
                            </li>
                            <div class="sup-options">
                                <div class="travelers__age-title">Возраст</div>
                                <div class="travelers__age-select">
                                    <select class="travelers__age">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select>
                                </div>
                            </div>
                            </li>
                        </ul>
                    </div>
                    <div class="insurance-filter__block">
                        <div class="insurance-filter__header">

                            <label for="holder-multipolicy" class="insurance-filter__title">
                                <input type="checkbox" class="filter-checkbox get-select__input" id="holder-multipolicy">
                                <div class="checkbox-label label-text">Годовой полис</div>
                            </label>
                        </div>
                        <div class="sup-options">
                            <ul class="input__items">
                                <li class="input__item">

                                    <label for="cb-multipolicy" class="filter-input__label">
                                        <input type="radio" class="filter-radio" id="cb-multipolicy" checked="" name="service-multipolicy-radio">
                                        <div class="label-text radio-label">Количество застрахованных дней в году: </div>
                                    </label>
                                </li>
                                <ul class="input__items cb-multipolicy__options">
                                    <li class="input__item">
                                        <label class="filter-input__label ">
                                            <input type="radio" class="filter-radio" name="service-multipolicy" value="30" checked="">
                                            <div class="label-text radio-label">30</div>
                                        </label>
                                    </li>
                                    <li class="input__item">

                                        <label class="filter-input__label">
                                            <input type="radio" class="filter-radio" name="service-multipolicy" value="45">
                                            <div class="label-text radio-label">45</div>
                                        </label>
                                    </li>
                                    <li class="input__item">

                                        <label class="filter-input__label">
                                            <input type="radio" class="filter-radio" name="service-multipolicy" value="60">
                                            <div class="label-text radio-label">60</div>
                                        </label>
                                    </li>
                                    <li class="input__item">

                                        <label class="filter-input__label">
                                            <input type="radio" class="filter-radio" name="service-multipolicy" value="90">
                                            <div class="label-text radio-label">90</div>
                                        </label>
                                    </li>
                                </ul>
                                <li class="input__item">

                                    <label for="unlimited-multipolicy" class="filter-input__label radio-label">
                                        <input type="radio" class="filter-radio" id="unlimited-multipolicy" name="service-multipolicy-radio">
                                        <div class="label-text radio-label"> Неограниченное количество поездок не более 90 дней каждая</div>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="insurance-filter__block">
                        <div class="input__item">
                            <label for="cb-abroad" class="insurance-filter__title">
                                <input type="checkbox" class="filter-checkbox" id="cb-abroad">
                                <div class="checkbox-label label-text">Я уже путешествую</div>
                            </label>
                        </div>
                    </div>
                    <div class="insurance-filter__block">
                        <div class="input__item">
                            <label class="insurance-filter__title">
                                <input type="checkbox" class="filter-checkbox">
                                <div class="checkbox-label label-text">Я не гражданин России </div>
                            </label>
                        </div>
                    </div>
                    <div class="basic-version">
                        <div class="insurance-filter__block">
                            <div class="insurance-filter__title-bg">
                                <div class="insurance-filter__title">БАЗОВЫЙ ВАРИАНТ СТРАХОВКИ</div>
                                <div class="insurance-filter__subtitle">достаточно для получения визы</div>
                                <div class="btn-group  btn-group-xs euro_usd">
                                    <button value="eur" type="button" class="changeCurrencyTrigger btn btn-sm btn-currency-eur active">
                                        <span class="glyphicon glyphicon-euro"></span>
                                    </button>
                                    <button value="usd" type="button" class="btn btn-sm btn-currency-usd changeCurrencyTrigger" data-original-title="" title="">
                                        <span class="glyphicon glyphicon-usd"></span>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="insurance-filter__block medicine">
                            <div class="insurance-filter__header">
                                <label for="cb-medicine" class="insurance-filter__title">
                                    <input id="cb-medicine" type="checkbox" class="filter-checkbox get-select__input" checked>
                                    <div class="checkbox-label label-text">Медицинское страхование</div>
                                </label>
                            </div>
                            <div class="medicine__content">
                                <ul class="input__items sup-options">
                                    <li class="input__item">
                                        <label class="filter-input__label">
                                            <input type="radio" class="filter-radio" name="service-medicine" checked>
                                            <div class="radio-label label-text">30 000 <span class="service-currency-icon">€</span></div>
                                        </label>
                                    </li>
                                    <li class="input__item">
                                        <label class="filter-input__label">
                                            <input type="radio" class="filter-radio" name="service-medicine">
                                            <div class="radio-label label-text">35 000 <span class="service-currency-icon">€</span></div>
                                        </label>
                                    </li>
                                    <li class="input__item">
                                        <label class="filter-input__label">
                                            <input type="radio" class="filter-radio" name="service-medicine">
                                            <div class="radio-label label-text">40 000 <span class="service-currency-icon">€</span></div>
                                        </label>
                                    </li>
                                </ul>
                                <div class="additional-text">
                                    <span class="med-more closed">Подробно</span>
                                    <div class="med-more-box">
                                        <span class="med-more-box__title">Входит в Вашу страховку:</span>
                                        <ul class="med-more__items">
                                            <li class="med-more__item">Прием врача по медицинским показаниям</li>
                                            <li class="med-more__item">Амбулаторное лечение</li>
                                        </ul>
                                        <span class="med-more-box__title">Дополнительно включить в полис:</span>
                                        <ul class="input__items med-more__box">
                                            <li class="input__item">
                                                <label for="cb-urgentStomatology" class="filter-input__label">
                                                    <input type="checkbox" class="filter-checkbox" id="cb-urgentStomatology">
                                                    <div class="checkbox-label label-text">Экстренная стоматология</div>
                                                </label>
                                            </li>
                                            <li class="input__item">
                                                <label for="insuredReturn" class="filter-input__label">
                                                    <input type="checkbox" class="filter-checkbox" id="insuredReturn">
                                                    <div class="checkbox-label label-text">Оплата проезда Застрахованного до места жительства после лечения в больнице</div>
                                                </label>
                                            </li>
                                            <li class="input__item">
                                                <label for="insuredEscortReturn" class="filter-input__label">
                                                    <input id="insuredEscortReturn" type="checkbox" class="filter-checkbox">
                                                    <div class="checkbox-label label-text">Оплата проезда Сопровождающему после лечения Застрахованного в больнице</div>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="insurance-filter__block sportGroup active">
                            <div class="insurance-filter__header">
                                <label for="cb-sportGroup" class="insurance-filter__title checkbox-label">
                                    <input id="cb-sportGroup" type="checkbox" class="filter-checkbox get-select__input">
                                    <div class="checkbox-label label-text">Занятие спортом и активный отдых</div>
                                </label>
                            </div>
                            <div class="sportGroup__box sup-options">
                                <div class="sport type-sports">
                                    <div class="sportGroup__subtitle closed">
                                        Любительский спорт и активный отдых 
                                    </div>
                                    <select class="multi__select-sport" multiple="multiple">
                                        <option>Горнолыжный спорт по маркированным трассам</option>
                                        <option>Сноубординг по маркированным трассам</option>
                                        <option>Горнолыжный спорт по не маркированным трассам (фрирайд)</option>
                                    </select>
                                </div>

                                <div class="sportCompetition type-sports">
                                    <div class="sportGroup__subtitle closed">
                                        Профессиональный спорт и соревнования 
                                    </div>
                                    <select class="multi__select-sport" multiple="multiple">
                                        <option>Горнолыжный спорт по маркированным трассам</option>
                                        <option>Сноубординг по маркированным трассам</option>
                                        <option>Горнолыжный спорт по не маркированным трассам (фрирайд)</option>
                                    </select>
                                </div>

                                <ul class="input__items">
                                    <li class="input__item motorcycle__line">

                                        <label for="motorcycle" class="filter-input__label">
                                            <input type="checkbox" class="filter-checkbox" id="motorcycle">
                                            <div class="checkbox-label label-text">Передвижение на мотоцикле/мопеде </div>
                                        </label>
                                    </li>
                                    <li class="input__item rescueActivities__line">

                                        <label for="rescueActivities" class="filter-input__label disabled checkbox-label">
                                            <input type="checkbox" class="filter-checkbox" id="rescueActivities">
                                            <div class="checkbox-label label-text">Поисково-спасательные мероприятия</div> 
                                        </label>
                                    </li>
                                    <li class="input__item searchActivities__line">

                                        <label for="searchActivities" class="filter-input__label disabled checkbox-label">
                                            <input type="checkbox" class="filter-checkbox" id="searchActivities">
                                            <div class="checkbox-label label-text">Эвакуация вертолетом </div>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>


                    <div class="dop-version">
                        <div class="insurance-filter__block-top">
                            <div class="insurance-filter__title-bg">
                                <div class="insurance-filter__title">ДОПОЛНИТЕЛЬНЫЕ ОПЦИИ</div>
                            </div>
                        </div>

                        <div class="insurance-filter__block accidents">
                            <div class="insurance-filter__header">

                                <label for="cb-accidents" class="insurance-filter__title">
                                    <input id="cb-accidents" type="checkbox" class="filter-checkbox get-select__input">
                                    <div class="checkbox-label label-text">Страхование от несчастных случаев</div>
                                </label>
                            </div>
                            <div class="accidents__box sup-options">
                                <ul class="input__items">
                                    <li class="input__item">

                                        <label for="cb-accident" class="filter-input__label">
                                            <input id="cb-accident" type="checkbox" class="filter-checkbox" name="service-accident">
                                            <div class="checkbox-label label-text">На все время путешествия</div>
                                        </label>
                                    </li>
                                    <ul class="input__items">
                                        <li class="input__item">

                                            <label class="filter-input__label">
                                                <input type="radio" class="filter-radio" name="service-accident">
                                                <div class="radio-label label-text">1 000 <span class="service-currency-icon">$</span></div>
                                            </label>
                                        </li>
                                        <li class="input__item">

                                            <label class="filter-input__label">
                                                <input type="radio" class="filter-radio" name="service-accident">
                                                <div class="radio-label label-text">3 000 <span class="service-currency-icon">$</span></div>
                                            </label>
                                        </li>
                                    </ul>
                                    <li class="input__item">

                                        <label for="aviaAccident" class="filter-input__label">
                                            <input type="checkbox" class="filter-checkbox" id="aviaAccident">
                                            <div class="checkbox-label label-text">На время авиаперелета</div>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="insurance-filter__block cargo">
                            <div class="insurance-filter__header">

                                <label for="cb-cargo" class="insurance-filter__title">
                                    <input id="cb-cargo" type="checkbox" class="filter-checkbox get-select__input">
                                    <div class="checkbox-label label-text">Страхование багажа</div>
                                </label>
                            </div>
                            <div class="cargo__box sup-options">
                                <ul class="input__items">
                                    <li class="input__item">

                                        <label for="cb-aviaCargo" class="filter-input__label">
                                            <input type="checkbox" class="filter-checkbox" checked="" id="cb-aviaCargo">
                                            <div class="checkbox-label label-text">Страхование багажа на время перелета</div>
                                        </label>
                                    </li>
                                    <ul class="input__items">
                                        <li class="input__item">

                                            <label class="filter-input__label">
                                                <input type="radio" class="filter-radio" name="service-aviaCargo">
                                                <div class="radio-label label-text">500 <span class="service-currency-icon">$</span></div>
                                            </label>
                                        </li>
                                        <li class="input__item">

                                            <label class="filter-input__label">
                                                <input type="radio" class="filter-radio" name="service-aviaCargo">
                                                <div class="radio-label label-text">1 000 <span class="service-currency-icon">$</span></div>
                                            </label>
                                        </li>
                                    </ul>
                                    <li class="input__item">

                                        <label for="cb-delayCargo" class="filter-input__label">
                                            <input type="checkbox" class="filter-checkbox" id="cb-delayCargo">
                                            <div class="checkbox-label label-text">Страхование задержки багажа</div>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="insurance-filter__block avia">
                            <div class="insurance-filter__header">

                                <label for="cd-avia" class="insurance-filter__title">
                                    <input id="cd-avia" type="checkbox" class="filter-checkbox get-select__input">
                                    <div class="checkbox-label label-text">Страхование авиаперелета</div>
                                </label>
                            </div>
                            <div class="avia__box sup-options">
                                <ul class="input__items">
                                    <li class="input__item">

                                        <label for="cb-regularDelay" class="filter-input__label">
                                            <input type="checkbox" class="filter-checkbox" checked id="cb-regularDelay">
                                            <div class="checkbox-label label-text">Страхование задержки регулярного рейса</div>
                                        </label>
                                    </li>
                                    <li class="input__item">

                                        <label for="charterDelay" class="filter-input__label">
                                            <input type="checkbox" class="filter-checkbox" id="charterDelay">
                                            <div class="checkbox-label label-text">Страхование задержки чартерного рейса</div>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="insurance-filter__block document">
                            <div class="input__item">

                                <label for="cd-document" class="insurance-filter__title">
                                    <input id="cd-document" type="checkbox" class="filter-checkbox">
                                    <div class="checkbox-label label-text">Страхование потери документов</div>
                                </label>
                            </div>
                        </div>

                        <div class="insurance-filter__block legal">
                            <div class="input__item">

                                <label for="cd-legal" class="insurance-filter__title checkbox-label">
                                    <input id="cd-legal" type="checkbox" class="filter-checkbox">
                                    <div class="checkbox-label label-text">Юридическая помощь</div>
                                </label>
                            </div>
                        </div>

                        <div class="insurance-filter__block tripCancel">
                            <div class="insurance-filter__header">

                                <label for="cd-tripCancel" class="insurance-filter__title">
                                    <input id="cd-tripCancel" type="checkbox" class="filter-checkbox get-select__input">
                                    <div class="checkbox-label label-text">Страхование отмены поездки</div>
                                </label>
                            </div>
                            <div class="tripCancel__box sup-options">
                                <span class="tripCancel__subtitle">на каждого путешественника</span>
                                <ul class="input__items">
                                    <li class="input__item">

                                        <label class="filter-input__label">
                                            <input type="radio" class="filter-radio" checked name="service-tripCancel">
                                            <div class="radio-label label-text">500
                                                <span class="service-currency-icon">$</span>
                                            </div>
                                        </label>
                                    </li>
                                    <li class="input__item">

                                        <label class="filter-input__label">
                                            <input type="radio" class="filter-radio" name="service-tripCancel">
                                            <div class="radio-label label-text">1000
                                                <span class="service-currency-icon">$</span>
                                            </div>
                                        </label>
                                    </li>
                                </ul>
                                <ul class="input__items">
                                    <li class="input__item">

                                        <label for="cb-visaCancel" class="filter-input__label">
                                            <input type="checkbox" class="filter-checkbox" id="cb-visaCancel">
                                            <div class="checkbox-label label-text">Страхование риска отказа в визе</div>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="insurance-filter__block civilLiability">
                            <div class="insurance-filter__header">

                                <label for="cd-civilLiability" class="insurance-filter__title">
                                    <input id="cd-civilLiability" type="checkbox" class="filter-checkbox get-select__input">
                                    <div class="checkbox-label label-text">Страхование гражданской ответственности </div>
                                </label>
                            </div>
                            <div class="civilLiability__box sup-options">
                                <ul class="input__items">
                                    <li class="input__item">

                                        <label class="filter-input__label">
                                            <input type="radio" class="filter-radio" name="service-civilLiability">
                                            <div class="radio-label label-text">10000
                                                <span class="service-currency-icon">$</span>
                                            </div>
                                        </label>
                                    </li>
                                    <li class="input__item">

                                        <label class="filter-input__label">
                                            <input type="radio" class="filter-radio" name="service-civilLiability">
                                            <div class="radio-label label-text">30000
                                                <span class="service-currency-icon">$</span>
                                            </div>
                                        </label>
                                    </li>
                                    <li class="input__item">

                                        <label class="filter-input__label">
                                            <input type="radio" class="filter-radio" name="service-civilLiability">
                                            <div class="radio-label label-text">50000
                                                <span class="service-currency-icon">$</span>
                                            </div>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="insurance-filter__block pregnancy">
                            <div class="insurance-filter__header">

                                <label for="cd-pregnancy" class="insurance-filter__title">
                                    <input id="cd-pregnancy" type="checkbox" class="filter-checkbox get-select__input">
                                    <div class="checkbox-label label-text">Страхование на случай осложнения беременности</div>
                                </label>
                            </div>
                            <div class="civilLiability__box sup-options">
                                <ul class="input__items">
                                    <li class="input__item">

                                        <label class="filter-input__label">
                                            <input type="radio" class="filter-radio" name="service-pregnancy">
                                            <div class="radio-label label-text"> до 12 недели</div>
                                        </label>
                                    </li>
                                    <li class="input__item">

                                        <label class="filter-input__label">
                                            <input type="radio" class="filter-radio" name="service-pregnancy">
                                            <div class="radio-label label-text">до 24-й недели</div>
                                        </label>
                                    </li>
                                    <li class="input__item">

                                        <label class="filter-input__label">
                                            <input type="radio" class="filter-radio" name="service-pregnancy">
                                            <div class="radio-label label-text">до 31-й недели</div>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="insurance-filter__block auto">
                            <div class="input__item">

                                <label for="cd-auto" class="insurance-filter__title">
                                    <input id="cd-auto" type="checkbox" class="filter-checkbox">
                                    <div class="checkbox-label label-text">Поездка на личном автомобиле</div>
                                </label>
                            </div>
                        </div>

                        <div class="insurance-filter__block work">
                            <div class="input__item">

                                <label for="cd-work" class="insurance-filter__title">
                                    <input id="cd-work" type="checkbox" class="filter-checkbox">
                                    <div class="checkbox-label label-text">Работа с повышенным риском</div>
                                </label>
                            </div>
                        </div>

                        <div class="insurance-filter__block assistance">
                            <div class="insurance-filter__header">

                                <label for="cd-assistance" class="insurance-filter__title">
                                    <input id="cd-assistance" type="checkbox" class="filter-checkbox get-select__input">
                                    <div class="checkbox-label label-text">Выбор по сервисной компании (ассистансу)</div>
                                </label>
                            </div>
                            <div class="assistance__box sup-options">
                                <ul class="input__items">
                                    <li class="input__item">

                                        <label for="assistance-europ" class="filter-input__label">
                                            <input type="checkbox" class="filter-checkbox" id="assistance-europ">
                                            <div class="checkbox-label label-text">Europ Assistance</div>
                                        </label>
                                    </li>

                                    <li class="input__item">

                                        <label for="assistance-class" class="filter-input__label">
                                            <input type="checkbox" class="filter-checkbox" id="assistance-class">
                                            <div class="checkbox-label label-text">Class Assistance</div>
                                        </label>
                                    </li>
                                    <li class="input__item">

                                        <label for="assistance-voyager" class="filter-input__label">
                                            <input type="checkbox" class="filter-checkbox" id="assistance-voyager">
                                            <div class="checkbox-label label-text">Global Voyager Assistance</div>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <div class="update-filter">
                        <button type="button" class="btn btn-light update-filter__btn" data-icon="" tabindex="1">Очистить фильтры</button>
                    </div>
                </div>
            </div>



        </div> 
    </div>
</div>
@endsection
