<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>Made Trip - туристический оператор</title>
        <link rel="shortcut icon" href="/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" />
        <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->

        <link rel="stylesheet" type="text/css" href="{{ asset('css/select2.min.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.mCustomScrollbar.min.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/insurance.css') }}" /> 
        <!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/autocomplete.css') }}"> -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
        <script src="{{ asset("js/jquery.selectric.js") }}" ></script>
    </head>
    <body class="results_tab">
        <!-- Header -->
        <header role="banner" class="mb-1">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-4 col-sm-2 col-3">
                        <div class="branding header__branding">
                            <div class="logo header__logo">
                                <div class="svg__wrapper">
                                    <a href="/">
                                        <img src="{{ asset('images/logo-white.png') }}">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 xol-lg-6 col-md-7 col-sm-9 col-7">
                        <!-- телефоны -->
                        <div class="header__contact">
                            <a class="header__phone" href="tel:+78007071558">8 (800) 707 15 58</a>
                            <a class="header__phone" href="tel:+74951313468">8 (495) 131 34 68</a>
                        </div>
                    </div>
                    <div class="header__nav col-xl-1 col-lg-1 col-md-1 col-sm-1 col-2">
                        <a class="nav__reveal" href="#">
                            <span class="icon--burger"></span>
                        </a>
                        <nav class="nav-main" role="navigation">
                            <ul role="menubar">
                                <li role="menuitem" tabindex="0">
                                    <a href="/">
                                        <span>Страхование</span>
                                    </a>
                                </li>
                            </ul>
                            <ul role="menubar" aria-label="other options menu">
                                <li role="menuitem" tabindex="0">
                                    <a href="#">
                                        <span>Поддержка</span>
                                    </a>
                                </li>
                            </ul>
                            <ul role="menubar" aria-label="user menu">
                                <li role="menuitem" aria-controls="user-menu" aria-expanded="false" rel="show">
                                    <a href="#" rel="modal" aria-expanded="false" aria-controls="modal--login-register">Кабинет</a>
                                </li>
                            </ul>
                            <ul role="menubar" aria-label="user menu">
                                <li role="menuitem" aria-controls="user-menu" aria-expanded="false" rel="show">
                                    <a href="/register" rel="modal" aria-expanded="false" aria-controls="modal--sign-up">Регистрация</a>
                                </li>
                            </ul>
                            <ul role="menu" class="panel user-menu" aria-hidden="true">
                                <li role="menuitem" tabindex="-1">
                                    <a href="#" rel="modal" aria-expanded="false" aria-controls="modal--login-register">Мой профиль</a>
                                </li>
                            </ul>
                        </nav>
                    </div>                
                </div>
            </div>
        </header>
        <div class="container-fluid" style="background: url('https://hungary-vac.ru/wp-content/themes/svc/img/path-bg.jpg') top repeat-x; height: 6px; "><div class="row"></div></div>
        @yield('content')
        <footer>
            <div class="container">
                <div class="row">

                    <div class="col-sm-2">
                        <img src="/images/3dsecure.png" style="height: 44px; width: 160px;" />
                    </div>

                    <div class="col-sm-10">
                        <hr/>
                        <div class="row float-right">
                            <div class="col-2 text-right"><span class="small">Москва&nbsp;&nbsp;</span></div>
                            <div class="col-9"><span class="font-weight-bold"> 8 (495) 131 34 68</span></div>
                            <div class="col-2 text-right"><span class="small">Россия&nbsp;&nbsp;</span></div>
                            <div class="col-9"><span class="font-weight-bold"> 8 (800) 707-15-58</span><span class="small"> (бесплатно)</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
        <script src="{{ asset('js/pickmeup.min.js') }}"></script>
        <script src="{{ asset('js/select2.full.min.js') }}"></script>
        <script src="{{ asset('js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>
    </body>
</html>