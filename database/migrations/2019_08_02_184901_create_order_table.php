<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id');
            $table->string('api_id');
            $table->longText('policy_info');
            $table->float('price');
            $table->string('pdf_file');
            $table->boolean('is_paid')->default(false);
            $table->string('transaction_id')->nullable();
            $table->dateTime("created_at");
            $table->dateTime("payed_at")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
