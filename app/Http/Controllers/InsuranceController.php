<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Carbon\CarbonInterval;

class InsuranceController extends Controller {

    public $country_list;

    public function index() {
        $data = [
            'countryList' => $this->getCountryList()
        ];
        return view('insurance.index', $data);
    }

    public function getCountryList() {
        $RGSAPI = new InsuranceAPIs\RosGosStrahAPI();
        if ($RGSAPI->isAuth()) {
            $raw = $RGSAPI->getClassifiers(['771254C3-CF3D-4A53-BD2F-0871264E71AB']);
            if (isset($raw[0])) {
                $list = [];
                $i = 0;
                foreach ($raw[0] as $country) {
                    $list [$country['CODE'] ? $country['CODE'] : $country['Name']] = [
                        'name_rus' => $country['Description'],
                        'name_eng' => isset($country['Name']) ? $country['Name'] : ''
                    ];
                }
                return $list;
            }
        }
        return [];
    }

    public function territoryTypes() {
        $RGSAPI = new RosGosStrahAPI();
        if ($RGSAPI->isAuth()) {
            $raw = $RGSAPI->getClassifiers(['5533EEBE-A8AF-4F25-A900-7A16E3AE0CBB']);
            //  dump($raw);
        }
    }

    public function search(Request $r) {
        $data = $r->all();
        $results = [];
        $policy = new \stdClass();

        //Поиск по РосГосСтраху
        $RGSAPI = new InsuranceAPIs\RosGosStrahAPI();
        if ($RGSAPI->isAuth()) {
            if (count($data) > 0) {
                $calculatedPolicy = $RGSAPI->calculatePolicy($data);
            }
            if (isset($calculatedPolicy) && count($calculatedPolicy) > 0) {
                //dd($calculatedPolicy);
                $price = $calculatedPolicy['RurPremium'];
                $policy->insured = count($calculatedPolicy['Insured']);
                $policy->countries = $calculatedPolicy['CountriesText'];
                $policy->date_from = Carbon::parse($calculatedPolicy['EffectiveDate'])->format('d.m.Y');
                $policy->date_to = Carbon::parse($calculatedPolicy['ExpirationDate'])->format('d.m.Y');
             
                $results [] = [
                    'price' => $price,
                    'apiID' => Config::get('api.rgs.apiID')
                ];
            }
        }
        //dd($calculatedPolicy);
        return view('insurance.results', ["results" => $results, 'policy' => $policy]);
    }

    public function checkout(Request $r) {
        if (!$r->post('apiID')) {
            return back();
        }
        $apiClassName = 'App\\Http\\Controllers\\InsuranceAPIs\\' . Config::get('api.' . $r->post('apiID') . '.className');

        $api = new $apiClassName;
        $policyInfo = $api->getCalculatedPolicy();
        if ($policyInfo) {
            //Session::put($r->post('apiID').'.calculatedPolicy', '');
            return view('insurance.checkout', ['policyInfo' => $policyInfo]);
        } else
            return \redirect('/');
    }

    public function payment(Request $r) {

        if ($r->post('apiID') && $r->post('insurerEmail')) {
            $apiID = $r->post('apiID');
            $policyInfo = Session::get($apiID . ".calculatedPolicy");

            if ($policyInfo) {

                $input = $r->post();
                $checkUser = User::where('email', '=', $input['insurerEmail'])->get();
                if (!count($checkUser)) {
                    $user_id = User::createUser([
                                'email' => $input['insurerEmail']
                    ]);
                } else
                    $user_id = $checkUser[0]->id;
                $order_id = Order::createOrder([
                            'user_id' => $user_id,
                            'api_id' => $r->post('apiID'),
                            'price' => $policyInfo['RurPremium']
                ]);

                $paymentData = [
                    'amount' => $policyInfo['RurPremium'],
                    'description' => 'Purchase insurance policy',
                    'fail_url' => url("/payment/fail"),
                    'merchant' => '93643af1-9385-4e75-891c-92cb450e6043',
                    'order_id' => $order_id,
                    'salt' => uniqid(),
                    'success_url' => url("/payment/success"),
                    'testing' => 1,
                    'unix_timestamp' => time(),
                ];
                $signature = $this->signature($paymentData);
                $paymentData['signature'] = $signature;

                $apiClassName = 'App\\Http\\Controllers\\InsuranceAPIs\\' . Config::get('api.' . $r->post('apiID') . '.className');

                $api = new $apiClassName;
                $preparedPolicy = $api->preparePolicy($input);
                Session::put($apiID . ".calculatedPolicy", $preparedPolicy);

                return view('insurance.payment', [
                    'order_id' => $order_id,
                    'price' => $policyInfo['RurPremium'],
                    'paymentData' => $paymentData
                ]);
            }
        }
        return \redirect('/');
    }

    public function paymentSuccess() {
        $transaction_id = Input::get("transaction_id");
        if ($transaction_id) {
            $paymentData = [
                'merchant' => '93643af1-9385-4e75-891c-92cb450e6043',
                'salt' => uniqid(),
                'testing' => 1,
                'transaction_id' => $transaction_id,
                'unix_timestamp' => time(),
            ];
            $paymentData['signature'] = $this->signature($paymentData);

            $transactionData = $this->getTransactionInfo($paymentData);
            if ($transactionData && $transactionData['status'] == 'ok') {
                $order = Order::where('id', '=', $transactionData['transaction']['order_id'])->first();
                if ($order) {
                    $apiClassName = 'App\\Http\\Controllers\\InsuranceAPIs\\' . Config::get('api.' . $order->api_id . '.className');
                    $api = new $apiClassName;
                    $preparedPolicy = Session::get($order->api_id . ".calculatedPolicy");
                    $apiRequest = $api->acceptPolicy($preparedPolicy);

                    if ($apiRequest['success']) {

                        $transaction = $transactionData['transaction'];
                        Order::where('id', '=', $order->id)->update([
                            'price' => $transaction['amount'],
                            'transaction_id' => $transaction['transaction_id'],
                            'policy_info' => json_encode($preparedPolicy, JSON_UNESCAPED_UNICODE),
                            'pdf_file' => $apiRequest['pdf'],
                            'is_paid' => true,
                            'payed_at' => $transaction['completed_datetime']
                        ]);
                    }
                    Session::put($order->api_id . ".calculatedPolicy", "");
                    return view('insurance.paymentSuccess', [
                        "success" => $apiRequest['success'],
                        "data" => $apiRequest['data'],
                        "errors" => $apiRequest['errors']
                    ]);
                }
            }
        }
    }

    private function double_sha1($data) {
        for ($i = 0; $i < 2; $i++) {
            $data = sha1('C1F045C4B5A37F24F19D8D0A653976DE' . $data);
        }
        return $data;
    }

    private function signature($data) {
        $signature = '';
        foreach ($data as $key => $value) {
            $signature .= $key . '=' . base64_encode($value) . '&';
        }
        return $this->double_sha1(rtrim($signature, "&"));
    }

    private function getTransactionInfo($paymentData) {
        $query = http_build_query($paymentData);
        $curl = curl_init("https://pay.modulbank.ru/api/v1/transaction/?" . $query);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);
        curl_close($curl);

        return json_decode($result, true);
    }

}
