<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FlightsController;
use App\Order;
use App\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    public function index() {
        $flightsAPI = new FlightsController();
        $data = [
            'countryList' => $this->getCountryList()
        ];
        return view('home.index', $data);
    }

    public function getCountryList() {
        $RGSAPI = new InsuranceAPIs\RosGosStrahAPI();
        if ($RGSAPI->isAuth()) {
            $raw = $RGSAPI->getClassifiers(['771254C3-CF3D-4A53-BD2F-0871264E71AB']);
            if (isset($raw[0])) {
                $list = [];
                $i = 0;
                foreach ($raw[0] as $country) {
                    $list [$country['CODE'] ? $country['CODE'] : $country['Name']] = [
                        'name_rus' => $country['Description'],
                        'name_eng' => isset($country['Name']) ? $country['Name'] : ''
                    ];
                }
                return $list;
            }
        }
        return [];
    }
}
