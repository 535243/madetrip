<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;

class FlightsController extends Controller
{

    private $config;
    private $apiKey;
    private $marker;

    public function __construct()
    {
        $this->config = Config::get('api.flights');
        $this->apiKey = $this->config['apiKey'];
        $this->marker = $this->config['marker'];

    }

    public function getCitiesList(Request $r) {
        $request = $r->get('term');
        if ($request) {
            $list = DB::table('cities')->where('name', 'like', $request.'%')->get();
            $responce = [];
            foreach ($list as $city) {
                $responce [] = [
                    'label' => $city->name.', '.$city->code,
                    'value' => $city->code
                ];
            }
            return json_encode($responce);
        }
        return '';
    }

    public function search(Request $r) {
        $data = [
           // 'currency' => 'RUB',
            'host' => 'madetrip.ru',
            'locale' => 'ru',
            'marker' => $this->marker,
            'passengers' => [
                'adults' => $r->get('adults')[0],
                'children' => $r->get('children')[0],
                'infants' => $r->get('infants')[0]
            ],
            'segments' => [
                [
                    'origin' => $r->get('fly-from'),
                    'destination' => $r->get('fly-to'),
                    'date' => date("Y-m-d", strtotime($r->get('departure_date')))
                ],
                [
                    'origin' => $r->get('fly-to'),
                    'destination' => $r->get('fly-from'),
                    'date' => date("Y-m-d", strtotime($r->get('arrival_date')))
                ]
            ],
            'trip_class' => $r->get('class'),
            'user_ip' => $_SERVER['REMOTE_ADDR'],
        ];
       // $signature_string = $this->apiKey.":".$data['currency'].":".$data['host'].":".$data['locale'].":".$data['marker'].":".$data['passengers']['adults'].":".$data['passengers']['children'].":".$data['passengers']['infants'].":".$data['segments'][0]['date'].":".$data['segments'][0]['destination'].":".$data['segments'][0]['origin'].":".$data['segments'][1]['date'].":".$data['segments'][1]['destination'].":".$data['segments'][1]['origin'].":".$data['trip_class'].":".$data['user_ip'];
        $data['signature'] = $this->getSignature($data, true);
        dump($data);
        dump($this->apiRequest($data, '/v1/flight_search', 'POST')); die();
    }
    private function getSignature(array $options, $withToken = false)
    {
        ksort($options);
        ksort($options['passengers']);
        foreach ($options['segments'] as $key => $value) {
            ksort($value);
            $options['segments'][$key] = implode(':', $value);
        }
        $options['passengers'] = implode(':', $options['passengers']);
        $options['segments'] = implode(':', $options['segments']);
        if ($withToken) $options = array_merge([$this->apiKey], $options);
        dump(implode(':', $options));
        return md5(implode(':', $options));
    }
    private function apiRequest($data, $url, $method='GET') {
        $host = $this->config['host'];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $host.$url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POST => $method == 'POST',
            CURLOPT_POSTFIELDS => $data ? json_encode($data) : [],
            CURLOPT_HTTPHEADER => array(
                "x-access-token: ".$this->apiKey,
                'Content-type: application/json',
                'Accept-Encoding' => 'gzip,deflate,sdch'
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
dump($response);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return json_decode($response, true);
        }
    }
}
