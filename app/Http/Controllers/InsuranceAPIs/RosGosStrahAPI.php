<?php

namespace App\Http\Controllers\InsuranceAPIs;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Carbon\CarbonInterval;

class RosGosStrahAPI extends Controller {

    private $userName;
    private $password;
    private $authCookie;
    private $isAuth;
    private $config;

    public function __construct() {
        $this->config = Config::get('api.rgs');

        $this->userName = $this->config['userName'];
        $this->password = $this->config['password'];
        $this->isAuth = false;

        $this->authCookie = $this->apiAuth();
    }

    public function isAuth() {
        return $this->isAuth;
    }

    private function apiAuth() {
        $apiData = [
            'userName' => $this->userName,
            'password' => $this->password,
            'createPersistentCookie' => 'true'
        ];
        $apiUrl = "/Authentication_JSON_AppService.axd/Login";
        $apiMethod = 'POST';

        $apiRequest = $this->apiRequest($apiData, $apiUrl, $apiMethod, false);
        if ($apiRequest['header'] && isset($apiRequest['header']['set-cookie'])) {
            $this->isAuth = true;
            return $apiRequest['header']['set-cookie'][0];
        }
        $this->isAuth = false;
        return false;
    }

    public function getProductId() {
        $url = "/ClientCardFeature/product/list.dat";
        $request = $this->apiRequest([], $url);
        return $request['body']['d'] ? ($request['body']['d']['IsValid'] ? $request['body']['d']['Result'][0]['ID'] : false) : false;
    }

    public function getClassifiers($ids) {
        $url = "/ClassifierFeature/Classifier.dat";
        $request = $this->apiRequest(['iDList' => $ids], $url);
        return isset($request['body']['d']['Result']) ? $request['body']['d']['Result'] : [];
    }

    public function calculatePolicy($request) {
        $people = [];
        foreach ($request['age'] as $age) {            
            $people[] = [
                'age' => $age
            ];
        }
        $countries = [];
        if (isset($request['countryList'])) {
            foreach ($request['countryList'] as $country) {
                $countries [] = $this->config['countryKeys'][$country];
            }
        } else
            $countries[] = $this->config['countryKeys']['Worldwide'];


        $date_from = Carbon::parse($request['date_from'])->format('Y-m-d') ? Carbon::parse($request['date_from'])->format('Y-m-d') : date('Y-m-d');
        $date_to = Carbon::parse($request['date_to'])->format('Y-m-d') ? Carbon::parse($request['date_to'])->format('Y-m-d') : date("Y-m-d", strtotime('+1 day', strtotime($date_from)));
        
        $prodId = $this->getProductId();
        $data = array(
            'policy' =>
            array(
                'ProductID' => $prodId,
                'PolicyType' => 'Single',
                'AmountCurrencyCd' => '43B13C6D-EEAF-4F35-A990-4236DCDAB212',
                'AmountCurrencyCODE' => 'EUR',
                'CurrencyRates' =>
                array(
                    0 =>
                    array(
                        'ID' => '4A1FDB9F-C12F-4B74-8D7D-70B4F0A57898',
                        'Name' => 'ДОЛЛАР США',
                        'CODE' => 'USD',
                        'Rate' => 62.866599999999998,
                        'CBRate' => 62.866599999999998,
                    ),
                    1 =>
                    array(
                        'ID' => '43B13C6D-EEAF-4F35-A990-4236DCDAB212',
                        'Name' => 'ЕВРО',
                        'CODE' => 'EUR',
                        'Rate' => 70.7941,
                        'CBRate' => 70.7941,
                    ),
                ),
                'Risks' =>
                array(
                    0 =>
                    array(
                        'RiskID' => 'BD9436B4-C102-4130-9451-E3F56CF126D4',
                        'VariantID' => 'F96E230D-FEDF-4600-8B9C-56FAECA76A37',
                        'CurrencyCODE' => 'EUR',
                        'TripType' => 'TripTypeDog',
                        'ContractPayed' => false,
                        'Residentship' => 'Resident',
                        'CustomFranchise' => "0 у.е.",
                        'SumInsured' => "",
                        'SumInsuredID' => "AAD88300-0A80-4BCA-88A5-503D2FBEE1B3"
                    ),
                    1 =>
                    array(
                        'RiskID' => 'E799CDB4-3C9D-4E60-97A6-9E07C7B86EF6',
                        'VariantID' => '45DA9E1F-A372-42D9-BB9A-4F9F398CD7C1',
                        'CurrencyCODE' => 'EUR',
                        'TripType' => 'TripTypeDog',
                        'ContractPayed' => false,
                        'Residentship' => 'Resident',
                        'SumInsured' => "",
                        'SumInsuredID' => "200C010B-214D-4229-891B-5124465A9C05"
                    ),
                ),
                'Insured' => $people,
                'IsStudent' => false,
                'InsurerForAll' => true,
                'EffectiveDate' => $date_from,
                'ExpirationDate' => $date_to,
                'Duration' => date_diff(date_create($date_from), date_create($date_to))->format("%a"),
                'FactEndDate' => $date_to,
                'Countries' => implode(",", $countries),
                'InsureFromCurrentDate' => false
            ),
            'isPortal' => true
        );
        $url = "/RGSTravelFeature/Calculate.cmd?id=7b142b93-5573-7e7f-98b5-ec8052d3b41f";
        $request = $this->apiRequest($data, $url, "POST");

        $result = isset($request['body']['d']['Result']) ? $request['body']['d']['Result'] : [];
        Session::put($this->config['apiID'] . '.calculatedPolicy', $result);

        return $result;
    }

    public function preparePolicy($data) {
        $prodId = $this->getProductId();
        $insured = [];
        $i = 0;
        foreach (array_combine($data['name'], $data['birthday']) as $name => $birthday) {
            $insured [] = [
                'Name' => $name,
                'Age' => $this->calculateAge($birthday),
                'BirthDate' => $birthday,
                'isInsurer' => (isset($input['isInsurer'][$i])) ? "true" : "false"
            ];
            $i++;
        }
        $insurer = [
            'Name' => $data['insurerName'],
            'BirthDate' => date("Y-m-d", strtotime($data['insurerBirthday']))
        ];
        $policyInfo = Session::get($this->config['apiID'] . ".calculatedPolicy");
        $requestData = array(
            'data' =>
            array(
                'ProductID' => $prodId,
                'PolicyType' => 'Single',
                'AmountCurrencyCd' => '43B13C6D-EEAF-4F35-A990-4236DCDAB212',
                'AmountCurrencyCODE' => 'EUR',
                'CurrencyRates' =>
                array(
                    0 =>
                    array(
                        'ID' => '4A1FDB9F-C12F-4B74-8D7D-70B4F0A57898',
                        'Name' => 'ДОЛЛАР США',
                        'CODE' => 'USD',
                        'Rate' => 62.866599999999998,
                        'CBRate' => 62.866599999999998,
                    ),
                    1 =>
                    array(
                        'ID' => '43B13C6D-EEAF-4F35-A990-4236DCDAB212',
                        'Name' => 'ЕВРО',
                        'CODE' => 'EUR',
                        'Rate' => 70.7941,
                        'CBRate' => 70.7941,
                    ),
                ),
                'Risks' =>
                array(
                    0 =>
                    array(
                        'RiskID' => 'BD9436B4-C102-4130-9451-E3F56CF126D4',
                        'VariantID' => 'F96E230D-FEDF-4600-8B9C-56FAECA76A37',
                        'CurrencyCODE' => 'EUR',
                        'TripType' => 'TripTypeDog',
                        'ContractPayed' => false,
                        'Residentship' => 'Resident',
                        'CustomFranchise' => "0 у.е.",
                        'SumInsured' => "",
                        'SumInsuredID' => "AAD88300-0A80-4BCA-88A5-503D2FBEE1B3"
                    ),
                    1 =>
                    array(
                        'RiskID' => 'E799CDB4-3C9D-4E60-97A6-9E07C7B86EF6',
                        'VariantID' => '45DA9E1F-A372-42D9-BB9A-4F9F398CD7C1',
                        'CurrencyCODE' => 'EUR',
                        'TripType' => 'TripTypeDog',
                        'ContractPayed' => false,
                        'Residentship' => 'Resident',
                        'SumInsured' => "",
                        'SumInsuredID' => "200C010B-214D-4229-891B-5124465A9C05"
                    ),
                ),
                'Insured' => $insured,
                'Insurer' => $insurer,
                'IsStudent' => false,
                'InsurerForAll' => true,
                'EffectiveDate' => $policyInfo['EffectiveDate'],
                'ExpirationDate' => $policyInfo['ExpirationDate'],
                'Duration' => date_diff(date_create($policyInfo['EffectiveDate']), date_create($policyInfo['ExpirationDate']))->format("%a"),
                'FactEndDate' => $policyInfo['FactEndDate'],
                'Countries' => $policyInfo['Countries'],
                'InsureFromCurrentDate' => false
            ),
            'isPortal' => true
        );
        return $requestData;
    }

    public function acceptPolicy($requestData) {


        $url = "/RGSTravelFeature/AcceptPolicy.cmd?id=b99699fa-43df-d981-917a-15ce29af79c6 ";
        $apiRequest = $this->apiRequest($requestData, $url, 'POST');

        $requestData = $apiRequest['body']['d'];
        if ($requestData['IsValid'] == true) {
            $saveRequest = $this->savePolicy($requestData['Result']);
            if (!$saveRequest['success']) {
                return [
                    "success" => false,
                    "errors" => isset($saveRequest['Errors']) ? $saveRequest['Errors'] : 'Неизвестная ошибка. Обратитесь к администратору',
                    "data" => []
                ];
            }
            $pdf = $this->sendEmail($saveRequest['data']['ID'], "inix.skjah@gmail.com");
            return [
                "pdf" => $pdf,
                "success" => true,
                "errors" => [],
                "data" => $requestData['Result']
            ];
        } else
            return [
                "success" => false,
                "errors" => isset($requestData['Errors']) ? $requestData['Errors'] : 'Неизвестная ошибка. Обратитесь к администратору',
                "data" => []
            ];
    }

    public function savePolicy($data) {
        $url = "/RGSTravelFeature/UpdatePolicy.cmd?id=b99699fa-43df-d981-917a-15ce29af79c6";
        $apiRequest = $this->apiRequest(["data" => $data], $url, "POST");

        $requestData = $apiRequest['body']['d'];
        if ($requestData['IsValid'] == true) {
            return [
                "success" => true,
                "errors" => [],
                "data" => $requestData['Result']
            ];
        } else
            return [
                "success" => false,
                "errors" => $requestData['Errors'],
                "data" => []
            ];
    }

    public function getCalculatedPolicy() {
        $policy = Session::get($this->config['apiID'] . '.calculatedPolicy');
        if ($policy) {
            $insured = [];
            foreach ($policy['Insured'] as $person) {
                $insured [] = [
                    'price' => $person['rurPremium']
                ];
            }
            return [
                'id' => $policy['ID'],
                'insured' => $insured,
                'price' => $policy['RurPremium'],
                'countries' => explode(",", $policy['CountriesText']),
                'dates' => [$policy['EffectiveDate'], $policy['ExpirationDate']],
                'apiID' => $this->config['apiID']
            ];
        } else
            return [];
    }

    private function sendEmail($policyId, $email) {
        $url = "/RGSTravelFeature/GetPrintForm.cmd";
        $pdfRequest = $this->apiRequest(["policyID" => $policyId], $url, 'POST', true, 20);

        if ($pdfRequest['body']['d']['IsValid']) {
            $pdf = $pdfRequest['body']['d']['Result'];
            $filename = 'police-' . uniqid() . '.pdf';
            $str = "";
            for ($i = 0; $i < strlen($pdf) - 3; $i += 3) {
                $dec = (int) ($pdf[$i] . $pdf[$i + 1] . $pdf[$i + 2]);
                $data = pack("C*", $dec);

                $str .= $data;
            }

            Storage::disk('uploads')->put($filename, $str);

            Mail::send('emails.payment_success', [], function ($m) use ($filename) {
                $m->from('hello@app.com', 'Your Application');
                $m->to("inix.skjah@gmail.com", "Inix")->subject('Your Reminder!');
                $m->attach(Storage::disk('uploads')->path($filename), []);
            });

            return $filename;
        }
        return false;
    }

    private function apiRequest($data, $url, $method = 'GET', $useAuth = true, $timeout = 2) {

        $host = $this->config['host'];

        $tries = 3;
        $headers = [];
        if ($url) {
            if ($method == 'POST')
                $requestHeaders = [
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen(json_encode($data))
                ];
            else
                $requestHeaders = [];

            if ($useAuth && isset($this->authCookie)) {
                $requestHeaders [] = 'Cookie: ' . $this->authCookie . ";";
            }
            if ($method == 'GET') {
                $requestHeaders [] = 'x-vs-parameters: ' . ($data ? json_encode($data) : 'null');
            }
            $ch = curl_init($host . $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $requestHeaders);
            curl_setopt($ch, CURLOPT_HEADERFUNCTION,
                    function($curl, $header) use (&$headers) {
                $len = strlen($header);
                $header = explode(':', $header, 2);
                if (count($header) < 2) // ignore invalid headers
                    return $len;

                $name = strtolower(trim($header[0]));
                if (!array_key_exists($name, $headers))
                    $headers[$name] = [trim($header[1])];
                else
                    $headers[$name][] = trim($header[1]);

                return $len;
            }
            );
            if ($method == 'POST') {
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            }
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

            $body = "";
            $i = 0;
            do {
                $result = curl_exec($ch);
                $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
                $body = substr($result, $header_size);
                $i++;
            } while (!$body && $i < $tries);

            curl_close($ch);
            return [
                'header' => $headers,
                'body' => json_decode($body, 1)
            ];
        }
        return false;
    }

    private function calculateAge($birthday) {
        return date_diff(date_create($birthday), date_create('now'))->y;
    }

}
