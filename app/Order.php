<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{

    protected $table = 'order';

    public $timestamps = null;

    public static function createOrder($data) {
        return DB::table('order')->insertGetId([
            'user_id' => $data['user_id'],
            'api_id' => $data['api_id'],
            'policy_info' => '',
            'price' => $data['price'],
            'pdf_file' => '',
            'is_paid' => false,
            'created_at' => date("Y-m-d H:i")
        ]);
    }
    public static function updateOrder($order_id, $data) {
        DB::table('order')->where('id', '=', $order_id)
        ->update([
            'price' => $data['price'],
            'pdf_file' => $data['pdf'],
            'is_paid' => $data['is_paid'],
            'transaction_id' => $data['transaction_id'],
            'payed_at' => $data['payed_at']
        ]);
    }
    public static function getOrderUserId($order_id) {
        return DB::table('order')->where('id', '=', $order_id)->get(['user_id'])->first()->user_id;
    }
}
