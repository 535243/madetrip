/**Навигация**/
$('.nav__reveal').on('click', function (e) {
    e.preventDefault();
    var elmthis = $(this);
    elmthis.toggleClass("is--open");
    elmthis.siblings("[role='navigation']").toggleClass("active");
    elmthis.parents("body").toggleClass("is--bgrnd");

});

/***Клик на любую область и закрытие**/
$(document).mouseup(function (e) {
    var container = $(".nav-main");
    if (container.has(e.target).length === 0 && !($(e.target).hasClass('icon--burger') || $(e.target).hasClass('nav__reveal'))) {
        container.removeClass('active');
        container.parents('.header__nav').find('.nav__reveal').removeClass('is--open');
    }

    var container = $(".search__field");
    if (container.has(e.target).length === 0) {
        container.removeClass('active');
    }

    var container = $(".search__field-dropdown");
    if (container.has(e.target).length === 0) {
        container.find('.dropdown').removeClass('is--open');
    }
});



/**Tabs для выбора формы**/
$('#menu-form li').on('click', function () {
    $('#menu-form li').removeClass('active');
    $(this).addClass('active');
    $('body').removeClass();

    if ($('.menu-form__flights').hasClass('active')) {
        ;
        $('body').addClass('flights-tab');
    };

    if ($('.menu-form__hotel').hasClass('active')) {
        $('body').addClass('hotel-tab');
    };

    if ($('.menu-form__cars').hasClass('active')) {
        $('body').addClass('cars-tab');
    };

    if ($('.menu-form__pack').hasClass('active')) {
        $('body').addClass('pack-tab');
    };

    if ($('.menu-form__Insurance').hasClass('active')) {
        $('body').addClass('Insurance-tab');
    };

});

/**Input вылет и прилет**/
$('.col--search input').on('focus', function () {
    $('.search__field').removeClass('active');
    var $this = $(this);
    $this.parents('.col--search').addClass('active');
});

$('#oneway').on('click', function () {
    if ($("#oneway").is(":checked")) {
        $(this).parents('.form-search__flights').find('.calendar__arrival').addClass('disabled');
    }
})

$('#roundtrip').on('click', function () {
    $(this).parents('.form-search__flights').find('.calendar__arrival').removeClass('disabled');
})


/** Календарь**/
$('[data-selector="pickmeup"]').on('focus', function () {
    $('.search__field').removeClass('active');
    $('.calendar').removeClass('active');
    var $this = $(this);
    var $pminputfield = $this.closest('.search__field');


    $this.parents('.calendar').addClass('active');
    $pminputfield.find('.dropdown').addClass('is--open');
});

$(document).ready(function () {
    $('.date__from').each(function () {
        pickmeup(this, {
            select_month: false,
            select_year: false,
            hide_on_select: true,
            position: 'bottom',
            flat: false,
            calendars: 2,
            default_date: false,
            class_name: 'pickmeup__calendar',
            format: "d.m.Y",
            min: new Date(),
            locale : 'ru',
            locales :
            {
                ru :{
                    days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                    daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                    daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                    monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
                },
            },
        });
    });
    $('.date__to').each(function () {
        pickmeup(this, {
            select_month: false,
            select_year: false,
            hide_on_select: true,
            position: 'bottom',
            flat: false,
            calendars: 2,
            default_date: false,
            class_name: 'pickmeup__calendar',
            format: "d.m.Y",
            min: new Date(),
            locale : 'ru',
            locales :
            {
                ru :{
                    days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                    daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                    daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                    monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
                },
            },
        });
    });

    $('.date__from').on('pickmeup-change', function (e) {
        if ($(this).hasClass('date__from-airtorts') && !$('#roundtrip').is(':checked')) {
            return false;
        }

        var obj = $(this).parents('.form--search').find('.date__to');
        pickmeup(obj[0]).show();
    })
});

/**Количество туристов**/

$('[data-traveler="traveler"]').on('focus', function () {
    $(this).parents('.search__field').find('.dropdown').addClass('is--open');
})

$('.selectric').on('click', function () {
    $(this).parents('.selectric-wrapper').addClass('selectric-open');
})

$('.selectric-scroll li').on('click', function () {
    $('.selectric-scroll li').removeClass('selected');
    $(this).addClass('selected');
    $(this).parents('.selectric-wrapper').find('.selectric__choose').html($(this).html());
    $(this).parents('.search__field').find('.form-input__choose').html($(this).html());
    $(this).parents('.selectric-wrapper').removeClass('selectric-open');
})

/**Количество комнат**/
$('#selectric-scroll__rooms li').on('click', function () {
    $(this).parents('.selectric-wrapper').find('.selectric__choose').html($(this).html());
    $('#rooms__number').html($(this).html());
    var count = $(this).data('index');

    $('.tabs__room-items').hide();
    $('.tabs__room-items').slice(0, count).show();
    $(".tabs__room").tabs("option", "active", 0);
})

$(function () {
    $(".tabs__room").tabs();
});
$('.select__child-age li').on('click', function () {
    $(this).addClass('selected');
    $(this).parents('.selectric-wrapper').find('.selectric__child-age').html($(this).html());
})

/**Плюс/минус**/

$(document).on("click", ".dd__group [data-type='increase']", function () {
    var current = $(this).closest(".dd__group").find("input").val();
    current++;
    if (current >= 99) {
        var current = 99;
    }
    var input = $(this).parents(".dd__group").find("input");
    if (input.data('name') == 'children') {
        if (current > 6) {
            current = 6;
        }
        roomChildren(this, current);
    }
    input.val(parseInt(current)).trigger('change');
    calcInput(this);
    insurNamber(this, current);
    // insurCount(this);
    return false;
});

$(document).on("click", ".dd__group [data-type='decrease']", function () {
    var current = $(this).closest(".dd__group").find("input").val();
    current--;
    if ('.insur__row-input') {
        if (current < 1) {
            current = 1;
        }
    } else {
        if (current < 0) {
            var current = 0;
        }
    }

    var input = $(this).parents(".dd__group").find("input");
    if (input.data('name') == 'children') {
        roomChildren(this, current);
    }
    input.val(parseInt(current)).trigger('change');
    calcInput(this);
    insurNamber(this, current);
    return false;
});

function calcInput(obj) {
    var sum = 0;
    $("input.dd__row-input").each(function () {
        sum += parseInt($(this).val());
    });

    $('#travelers__number').html(sum);
}

function roomChildren(obj, count) {
    $(obj).parents('.tabs__content').find('.select__child-age').hide();
    $('.select__child-age').slice(0, count).show();
}

function insurNamber(obj, count) {
    $('#insur__number').html(count);
    var parent = $('.insur__age-list');
    if (parent.children().length < count) {
        parent.find('.dd__row.insur__age').last().clone().appendTo(parent);
    } else if (parent.children().length > count) {
        parent.find('.dd__row.insur__age').last().remove();
    }

    // $(obj).parents('.insur__dropdown').find('.insur__age').hide();
    // $('.insur__age').slice(0, count).show();
}

/***autocomplete
 $( function() {
 var availableTags = [
 "Москва",
 "Вена",
 "Мюнхен",
 "Бангкок",
 "Гаваи"
 ];
 $( ".autocomplete__input" ).autocomplete({
 source: availableTags
 });
 } );***/


(function ($) {
    $.widget("ui.combobox", {
        _create: function () {
            var input, self = this,
                    select = this.element.hide(),
                    selected = select.children(":selected"),
                    value = selected.val() ? selected.text() : "",
                    wrapper = $("<span>").addClass("ui-combobox").insertAfter(select);

            input = $("<input>").appendTo(wrapper).val(value).addClass("ui-state-default").autocomplete({
                delay: 0,
                minLength: 0,
                source: function (request, response) {
                    var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                    response(select.children("option").map(function () {
                        var text = $(this).text();
                        if (this.value && (!request.term || matcher.test(text)))
                            return {
                                label: text.replace(
                                        new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
                                value: text,
                                option: this
                            };
                    }));
                },
                select: function (event, ui) {
                    ui.item.option.selected = true;
                    self._trigger("selected", event, {
                        item: ui.item.option
                    });

                },
                change: function (event, ui) {
                    if (!ui.item) {
                        var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i"),
                                valid = false;
                        select.children("option").each(function () {
                            if ($(this).text().match(matcher)) {
                                this.selected = valid = true;
                                return false;
                            }
                        });
                        if (!valid) {
                            // remove invalid value, as it didn't match anything
                            $(this).val("");
                            select.val("");
                            input.data("autocomplete").term = "";
                            return false;
                        }
                    }
                }
            }).addClass("ui-widget ui-widget-content ui-corner-left");

            input.data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li></li>").data("item.autocomplete", item).append("<a>" + item.label + "</a>").appendTo(ul);
            };
            /*
             input.click(function() {
             // close if already visible
             if (input.autocomplete("widget").is(":visible")) {
             input.autocomplete("close");
             return;
             }
             
             // work around a bug (likely same cause as #5265)
             $(this).blur();
             
             // pass empty string as value to search for, displaying all results
             input.autocomplete("search", "");
             input.focus();
             });*/
        },

        destroy: function () {
            this.wrapper.remove();
            this.element.show();
            $.Widget.prototype.destroy.call(this);
        }
    });
})(jQuery);

$(function () {
    $("#combobox").combobox({
        selected: function (event, ui) {
        }
    });
    $(".makeSelectric").selectric();
});



$('.travelers__item-number .get-select__input').on('click', function () {
    if ($(this).parents('.travelers__item').find('.get-select__input').prop('checked')) {
        $(this).parents('.travelers__item').find('.sup-options').slideToggle(300);
    } else {
        $(this).parents('.travelers__item').find('.sup-options').slideToggle(300);
    }
})

$('.insurance-filter__header .get-select__input').on('click', function () {
    if ($(this).parents('.insurance-filter__block').find('.get-select__input').prop('checked')) {
        $(this).parents('.insurance-filter__block').find('.sup-options').slideToggle(300);
    } else {
        $(this).parents('.insurance-filter__block').find('.sup-options').slideToggle(300);
    }
})


$('.medicine .insurance-filter__header .get-select__input').on('click', function () {
    if ($(this).parents('.insurance-filter__block').find('.get-select__input').prop('checked')) {
        $(this).parents('.insurance-filter__block').find('.medicine__content').slideToggle(300);
        $(this).parents('.basic-version').find('.sportGroup').addClass('active');
    } else {
        $(this).parents('.insurance-filter__block').find('.medicine__content').slideToggle(300);
        $(this).parents('.basic-version').find('.sportGroup').removeClass('active');
    }
})

$('#cb-multipolicy').on('click', function () {
    if ($(this).parents('.input__items').find('#cb-multipolicy').prop('checked')) {
        $(this).parents('.input__items').find('.cb-multipolicy__options').slideToggle(300);
    } else {
        $(this).parents('.input__items').find('.cb-multipolicy__options').slideToggle(300);
    }
})

$('#unlimited-multipolicy').on('click', function () {
    if ($(this).parents('.input__items').find('#unlimited-multipolicy').prop('checked')) {
        $(this).parents('.input__items').find('.cb-multipolicy__options').slideToggle(300);
    } else {
        $(this).parents('.input__items').find('.cb-multipolicy__options').slideToggle(300);
    }
})


$('#motorcycle').on('click', function () {
    if ($(this).prop('checked')) {
        $(this).parents('.input__items').find('.rescueActivities__line').addClass('active');
        $(this).parents('.input__items').find('.searchActivities__line').addClass('active');
    } else {
        $(this).parents('.input__items').find('.rescueActivities__line').removeClass('active');
        $(this).parents('.input__items').find('.searchActivities__line').removeClass('active');
        $(this).parents('.input__items').find('#rescueActivities').prop('checked', false);
        $(this).parents('.input__items').find('#searchActivities').prop('checked', false);
    }
})

$('.changeCurrencyTrigger').on('click', function () {
    $('.changeCurrencyTrigger').removeClass('active');
    $(this).addClass('active');
})

$('.med-more').on('click', function () {
    $(this).toggleClass('closed');
    $(this).parents('.additional-text').find('.med-more-box').slideToggle(300);
})

$('.filter-travelers__title').on('click', function () {
    $(this).toggleClass('closed');
    $(this).parents('.filter-travelers').find('.travelers__items').slideToggle(300);
})

$('.filters-county__title').on('click', function () {
    $(this).toggleClass('closed');
    $(this).parents('.filters-county').find('.select2-container').slideToggle(300);
})

$('.send-mail__title').on('click', function () {
    $(this).toggleClass('closed');
    $(this).parents('.send-to-mail').find('.send-to-mail__block').slideToggle(300);
})

$(".multi__select-sport").select2({
    placeholder: "Укажите вид спорта",
    tags: true
});
$(".multi__select-country").select2({
    placeholder: "Название страны",
    tags: true
});

$('.sportGroup__subtitle').on('click', function () {
    $(this).toggleClass('closed');
    $(this).parents('.type-sports').find('.select2-container').slideToggle(300);
})

$('.filters-period__title').on('click', function () {
    $(this).toggleClass('closed');
    $(this).parents('.filters-period').find('.filters-period__calendar').slideToggle(300);
})

/** Календарь для страницы с результатом**/

$(document).ready(function () {
    $('.filters-period__from').each(function () {
        pickmeup(this, {
            select_month: false,
            select_year: false,
            hide_on_select: true,
            position: 'bottom',
            flat: false,
            calendars: 1,
            default_date: false,
            class_name: 'pickmeup__calendar',
            //format: "a d b",
            min: new Date(),
            locale : 'ru',
            locales :
            {
                ru :{
                    days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                    daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                    daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                    monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
                },
            },
        });
    });
    $('.filters-period__to').each(function () {
        pickmeup(this, {
            select_month: false,
            select_year: false,
            hide_on_select: true,
            position: 'bottom',
            flat: false,
            calendars: 1,
            default_date: false,
            class_name: 'pickmeup__calendar',
            //format: "a d b",
            min: new Date(),
            locale : 'ru',
            locales :
            {
                ru :{
                    days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                    daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                    daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                    monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
                },
            },
        });
    });

    $('.filters-period__from').on('pickmeup-change', function (e) {
        /*if($(this).hasClass('date__from-airtorts') && !$('#roundtrip').is(':checked')) {
         return false;
         }*/

        var obj = $(this).parents('.filters-period__calendar').find('.filters-period__to');
        pickmeup(obj[0]).show();
    })
});


$(document).ready(function () {
    //open/close lateral filter
    function triggerFilter($bool) {
        var elementsToTrigger = $([$('.filter-trigger'), $('.insurance-filter')]);
        elementsToTrigger.each(function(){
            $(this).toggleClass('filter-is-visible', $bool);
        });
    }
    
    $('.filter-trigger').on('click', function () {
        triggerFilter(true);
    });
    $('.filter-close').on('click', function () {
        triggerFilter(false);
    });


    //mobile version - detect click event on filters tab
    var filter_tab_placeholder = $('.filter-nav__placeholder span'),
            filter_tab_placeholder_default_value = 'Страница',
            filter_tab_placeholder_text = filter_tab_placeholder.text();

    $('.filter-nav li').on('click', function (event) {
        //detect which tab filter item was selected
        var selected_filter = $(event.target).data('type');

        //check if user has clicked the placeholder item
        if ($(event.target).is(filter_tab_placeholder)) {
            (filter_tab_placeholder_default_value == filter_tab_placeholder.text()) ? filter_tab_placeholder.text(filter_tab_placeholder_text) : filter_tab_placeholder.text(filter_tab_placeholder_default_value);
            $('.filter-nav__inner').toggleClass('is-open');

            //check if user has clicked a filter already selected 
        } else if (filter_tab_placeholder.data('type') == selected_filter) {
            filter_tab_placeholder.text($(event.target).text());
            $('.filter-nav__inner').removeClass('is-open');

        } else {
            //close the dropdown and change placeholder text/data-type value
            $('.filter-nav__inner').removeClass('is-open');
            filter_tab_placeholder.text($(event.target).text()).data('type', selected_filter);
            filter_tab_placeholder_text = $(event.target).text();
            /*
             //add class selected to the selected filter item
             $('.filter-nav__inner .active').removeClass('active');
             $(event.target).addClass('active');*/
        }
    });
    
    
    
    //fix lateral filter and gallery on scrolling
    $(window).on('scroll', function(){
        fixContent();
        (!window.requestAnimationFrame) ? fixContent() : window.requestAnimationFrame(fixContent);
    });
    
    function fixContent() {
        var offsetTop = $('.result__page').offset().top;
        scrollTop = $(window).scrollTop();
        //console.log(scrollTop);
//        console.log(offsetTop);
      (scrollTop >= offsetTop - 20 ) ? $('.result__page').addClass('is-fixed') : $('.result__page').removeClass('is-fixed');
    }
    
   /**Сбросить фильтр**/
   
   $('.update-filter__btn').on('click', function() {
       $('input').prop('checked', false);
   })
    
   
});